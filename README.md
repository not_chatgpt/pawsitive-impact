# Module3 Project Gamma

## About The Project
Our FastAPI application, Pawsitive Impact, facilitates seamless CRUD operations for pet adoptions.

## User Stories
- Customers: Anyone adopting, or anyone advertising for adoption
- Needs: Simplifies the adoption process
- Features:
  - As a user, I can login so I can access restricted features
  - As a user, I can access account information and edit or delete my user info
  - As a user, I can post or edit my own pet information regarding adoption
  - As a user, I can leave “I’m interested” feedback on a posting
  - As a user, I can search for other adoption shelters near me
## Intended Market

Aimed at all animal lovers looking to add another furry member to their fur-ever family.
## Stretch Goals

- Message other users if one was interested in adopting specific pet
- Leave an “I’m interested” feedback on post and receive an automated email to the poster.
- Create a page dedicated to fostering pets
- Create a page dedicated to lost pets
- Sign up/in with gmail account
- Create a dark mode for application
- Add cancel button when creating a post and have it redirect to appropriate page
- Expand on search bar to filter by:
  - location / mile radius
  - pet breed
  - Size
  - Age

## OnBoarding
1. Fork the repo:
https://gitlab.com/not_chatgpt/pawsitive-impact

2. Clone the repository:
https://gitlab.com/not_chatgpt/pawsitive-impact.git
3. Navigate into the repository
4. Install dependencies via ‘npm install’
5. Checkout to a feature branch
6. Run the application using Docker:
docker compose up
7. Access application in browser:
http://localhost:3000

## Install Extensions
- Prettier: <https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode>
- Black Formatter: <https://marketplace.visualstudio.com/items?itemName=ms-python.black-formatter>

## Tech Stack
This application was build with the following technologies, frameworks, and libraries:
-React
-FastAPI
-PostgreSQL
-Docker
-AOS

## Journaling
-Maintain a dev log in Journal folder

## Documentation
![adoption page](images/wireframe-adoption-page.png)
![landing page](images/landing-page.png)
![profile page and create post page](images/profile-and-create-post.png)
S

## Collaboration Approach
Our team has embraced mob programming as our primary collaboration method. Most of our challenges were tackled in real-time through live screen sharing. In addition to this, we've integrated GitLab's issue tracking system into our workflow to efficiently manage and resolve any outstanding issues.

## Issue Tracking
- [ ] Signup/login backend authentication
- [ ] Developer page
- [ ] CI/CD
- [ ] Find shelter API
- [ ] Signup page
- [ ] Video Formatting
- [ ] Link routes in App.js to the Nav.js

#### User stories
- [ ] Update Post
- [ ] Delete Post
- [ ] Create Post

## Testing
- Bira Luiz: api/tests/test_accounts (test_create_account)
- Efren: api/tests/test_accounts (test_get_all_accounts)
- Linh: api/tests/test_posts (test_get_non_adopted_posts)
- Mina: api/tests/test_posts (test_get_adopted_posts)
- Weston: api/tests/test_posts (test_get_all_posts)
