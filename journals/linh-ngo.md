<!-- In the journals, every day that you work on the project, you must make an entry in your journal after you've finished that day. At a minimum, you'll need to include the following information in each entry:

The date of the entry
A list of features/issues that you worked on and who you worked with, if applicable
A reflection on any design conversations that you had
At least one ah-ha! moment that you had during your coding, however small
Keep your journal in reverse chronological order. Always put new entries at the top.

Here's a sample of the first two entries of someone's journal file:

## July 9, 2021

Today, I worked on:

* Getting a customer's data for the account page
  with Asa

Asa and I wrote some SQL. We tested it out with
Insomnia and the UI. We had to coordinate with
Petra, who was working on the GHI with Azami.

Today, I found the F2/Rename symbol functionality
in Visual Studio Code! It allows me to rename a
variable without causing bugs. I am going to be
using this a lot!

## July 7, 2021

Today, I worked on:

* Signing up a customer with Petra

Petra and I had a hard time figuring out the logic
for what happens when a customer signs up. We
finally came to the conclusion that not only did we
have to create the `User` record, but we also needed
to create associated preference records **and** had
to log them in.

Today, database transactions finally clicked. It
happened while we were trying to insert the
preferences for the customer. We had a bug in the
code, so the `User` record got created, but none
of the preferences. All of the inserts should
succeed or fail together. So, we used a transaction
to do that. -->


### WEEK 17:
##### 20231023:
* updated Nav and video over weekend
* worked on Post details

### WEEK 16:
##### 20231019:
* updated Nav, App routes
* added Navigation to all adopt buttons
* fixed css image size in cards on adopt/success

##### 20231018:
* adopt/success css formatting

##### 20231017:
* unit tests completed
* functionality Adopt and Success Page completed (needs CSS)

##### 20231016:
* updated main page css (video, carousel, team page)
* frontend authentication, signup and login page


### WEEK 15:
##### 20231013:
* completed skelton for main page and nav, need to fix team.js

##### 20231012:
* completed carousel on main page (need to work on css)

##### 20231010:
* internet went out, team completed remaining CRUD for posts

##### 20231009:
* worked on create posts (routers.py, queries.py, main.py), resolved errors for foreign key


### WEEK 14:
##### 20230928:
* updated accounts login to use username
* completed CRUD functions for Account authentication

##### 20230927:
* removed unnecessary dependencies from requirements.txt
* created directories and files
  * queries/accounts.py
  * queries/pool.py
  * routers/accounts.py
  * authenticator.py
* class Accounts, AccountsOut, AccountOutWithHashedPassword
* Successfully created authentication, accounts can be created and logged in via email

##### 20230926:
* created init.sql
* merged to main (gave error in ghi)
  * added main.py back into api
  * corrected typo in docker-compose.yaml (GBT to GPT)
  * update requirements.txt, was deprecated and needed most recent update.
    (fastapi[all]==0.78.0 changed to fastapi[all]>=0.78.0)

##### 20230925:
* created Linh-branch
* attempted JWTdown
* registered database (Postgres)
* installed pg-admin
