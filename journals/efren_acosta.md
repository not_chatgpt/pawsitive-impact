09/25/2023
Registered database and installed PGadmin

09/26/2023
Created table for accounts and changed the database url to follow init.sql

09/27/2023
Finished setting up the log in and log out and create user for authentication.

09/28/2023
Changed login from email to username.
Finished CRUD for accounts.
