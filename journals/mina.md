9/25/2023 -- Had to leave at 4PM CT due to dog being in ICU.

9/26/2023 -- Created my own branch. Created table and database for accounts following init.sql. Fixed my bug in ghi container by installing npm. Troubleshot Docker error.

9/27/2023 -- Successfully created authentication with entire team via email instead of username.

9/28/2023 -- Fixed authentification with entire team from email to username with the help of Violet. Also finished CRUD for accounts. We will finish CRUD for post after the break.

10/9/2023 -- Successfully coded the create portion (from CRUD) for posts. We will figure out how to secure it in the backend tomorrow. 

10/10/2023 -- Fixed create portion of Posts and secured it. Finished up CRUD for posts.

10/11/2023 -- Completed our backend! Started our frontend!

10/12/2023 -- Got carousel working today. Also started working on the structure of our home page.

10/13/2023 --

10/16/2023 --

10/17/2023 -- Split up react frontend work.

10/18/2023 -- Created footer.