from fastapi.testclient import TestClient
from main import app
from queries.posts import PostRepo


client = TestClient(app)


class MockPostQueries:
    fake_db = {
        "posts": [
            {
                "id": 1,
                "pet_name": "Buddy",
                "species": "Dog",
                "color": "Brown",
                "age": "2",
                "picture_url": "https://test.com",
                "pet_size": "Small",
                "location_city": "San Diego",
                "location_state": "CA",
                "location_zipcode": 00000,
                "pet_description": "This is a test description",
                "is_adopted": False,
                "author_id": 1,
                "interested": 0,
            },
            {
                "id": 2,
                "pet_name": "Fluffy",
                "species": "Cat",
                "color": "White",
                "age": "3",
                "picture_url": "https://test.com",
                "pet_size": "Medium",
                "location_city": "Los Angeles",
                "location_state": "CA",
                "location_zipcode": 90001,
                "pet_description": "This is another test description",
                "is_adopted": True,
                "author_id": 2,
                "interested": 0,
            }
        ]
    }

    def get_all(self):
        try:
            return self.fake_db["posts"]
        except Exception as e:
            return {"message": str(e)}

    def get_all_adopted(self):
        try:
            return self.fake_db["posts"]
        except Exception as e:
            return {"message": str(e)}

    def get_all_not_adopted(self):
        try:
            return self.fake_db["posts"]
        except Exception as e:
            return {"message": str(e)}


def test_get_all_posts():
    # Arrange
    app.dependency_overrides[PostRepo] = MockPostQueries
    # Act
    response = client.get("/api/posts/")

    # Assert
    assert response.status_code == 200
    assert response.json() == [
        {
            "id": 1,
            "pet_name": "Buddy",
            "species": "Dog",
            "color": "Brown",
            "age": "2",
            "picture_url": "https://test.com",
            "pet_size": "Small",
            "location_city": "San Diego",
            "location_state": "CA",
            "location_zipcode": 00000,
            "pet_description": "This is a test description",
            "is_adopted": False,
            "author_id": 1,
            "interested": 0,
        },
        {
            "id": 2,
            "pet_name": "Fluffy",
            "species": "Cat",
            "color": "White",
            "age": "3",
            "picture_url": "https://test.com",
            "pet_size": "Medium",
            "location_city": "Los Angeles",
            "location_state": "CA",
            "location_zipcode": 90001,
            "pet_description": "This is another test description",
            "is_adopted": True,
            "author_id": 2,
            "interested": 0,
        }
        # created by Weston Gambardella
    ]


def test_get_adopted_posts():
    app.dependency_overrides[PostRepo] = MockPostQueries
    # Act
    response = client.get("/api/posts/adopted/")

    # Assert
    assert response.status_code == 200
    assert response.json() == [
        {
            "id": 1,
            "pet_name": "Buddy",
            "species": "Dog",
            "color": "Brown",
            "age": "2",
            "picture_url": "https://test.com",
            "pet_size": "Small",
            "location_city": "San Diego",
            "location_state": "CA",
            "location_zipcode": 00000,
            "pet_description": "This is a test description",
            "is_adopted": False,
            "author_id": 1,
            "interested": 0,
        },
        {
            "id": 2,
            "pet_name": "Fluffy",
            "species": "Cat",
            "color": "White",
            "age": "3",
            "picture_url": "https://test.com",
            "pet_size": "Medium",
            "location_city": "Los Angeles",
            "location_state": "CA",
            "location_zipcode": 90001,
            "pet_description": "This is another test description",
            "is_adopted": True,
            "author_id": 2,
            "interested": 0,
        }
        # Created by Mina Nguyen
    ]


def test_get_non_adopted_posts():
    app.dependency_overrides[PostRepo] = MockPostQueries
    # Act
    response = client.get("/api/posts/not-adopted/")

    # Assert
    assert response.status_code == 200
    assert response.json() == [
        {
            "id": 1,
            "pet_name": "Buddy",
            "species": "Dog",
            "color": "Brown",
            "age": "2",
            "picture_url": "https://test.com",
            "pet_size": "Small",
            "location_city": "San Diego",
            "location_state": "CA",
            "location_zipcode": 00000,
            "pet_description": "This is a test description",
            "is_adopted": False,
            "author_id": 1,
            "interested": 0,
        },
        {
            "id": 2,
            "pet_name": "Fluffy",
            "species": "Cat",
            "color": "White",
            "age": "3",
            "picture_url": "https://test.com",
            "pet_size": "Medium",
            "location_city": "Los Angeles",
            "location_state": "CA",
            "location_zipcode": 90001,
            "pet_description": "This is another test description",
            "is_adopted": True,
            "author_id": 2,
            "interested": 0,
        }
        # Created by Linh Ngo
    ]
