from fastapi.testclient import TestClient
from main import app
from queries.accounts import AccountRepo


client = TestClient(app)


class MockAccountQueries:
    fake_db = {
        "accounts": [
            {
                "id": 1,
                "first_name": "John",
                "last_name": "Doe",
                "email": "test@test.com",
                "username": "test",
            },
            {
                "id": 2,
                "first_name": "Jane",
                "last_name": "Doe",
                "email": "test2@test.com",
                "username": "test2",
            },
        ]
    }

    def get_all(self):
        try:
            return self.fake_db["accounts"]
        except Exception as e:
            return {"message": str(e)}


def test_get_all_accounts():
    # Arrange
    app.dependency_overrides[AccountRepo] = MockAccountQueries
    # Act
    response = client.get("/api/accounts")

    # Assert
    assert response.status_code == 200
    assert response.json() == [
        {
            "id": 1,
            "first_name": "John",
            "last_name": "Doe",
            "email": "test@test.com",
            "username": "test",
        },
        {
            "id": 2,
            "first_name": "Jane",
            "last_name": "Doe",
            "email": "test2@test.com",
            "username": "test2",
        },
    ]


class MockCreateAccountQuery:
    fake_db = {"accounts": []}

    def create_account(self, data, hashed_password):
        try:
            account = {
                "id": len(self.fake_db["accounts"]) + 1,
                "first_name": data.first_name,
                "last_name": data.last_name,
                "email": data.email,
                "username": data.username,
                "password": hashed_password,
            }
            self.fake_db["accounts"].append(account)
            return account
        except Exception as e:
            return {"message": str(e)}


def test_create_account():
    # Arrange
    app.dependency_overrides[AccountRepo] = MockCreateAccountQuery

    account_data = {
        "first_name": "Alice",
        "last_name": "Smith",
        "email": "alice@example.com",
        "username": "alice",
        "password": "secure_password",
    }

    # Act
    response = client.post("/api/accounts", json=account_data)

    # Assert
    assert response.status_code == 200

    assert response.json()["first_name"] == account_data["first_name"]
    assert response.json()["last_name"] == account_data["last_name"]
    assert response.json()["email"] == account_data["email"]
    assert response.json()["username"] == account_data["username"]

    app.dependency_overrides = {}
    # Created by Bira Luiz
