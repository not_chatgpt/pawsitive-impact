from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers import accounts, posts
import os
from authenticator import authenticator

# REACT_APP_API_HOST = os.environ["REACT_APP_API_HOST"]
# CORS_HOST = os.environ["CORS_HOST"]
# print("This is the host", REACT_APP_API_HOST)
app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=[os.environ.get("CORS_HOST", "REACT_APP_API_HOST")],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "module": 3,
            "week": 17,
            "day": 5,
            "hour": 19,
            "min": "00",
        }
    }


app.include_router(authenticator.router, tags=["login/logout"])
app.include_router(accounts.router, tags=["accounts"])
app.include_router(posts.router, tags=["posts"])
