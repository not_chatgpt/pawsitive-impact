from pydantic import BaseModel
from queries.pool import pool
from typing import Union


class Error(BaseModel):
    pass


class DuplicateAccountError(ValueError):
    pass


class Account(BaseModel):
    first_name: str
    last_name: str
    email: str
    username: str
    password: str


class AccountOut(BaseModel):
    first_name: str
    last_name: str
    email: str
    username: str
    id: int


class AccountOutWithHashedPassword(AccountOut):
    hashed_password: str


class AccountRepo:
    def get_all(self) -> Union[Error, list[AccountOut]]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, first_name, last_name, email, username
                    FROM accounts
                    ORDER BY id;
                    """,
                )
                result = []
                for record in cur:
                    account = AccountOut(
                        id=record[0],
                        first_name=record[1],
                        last_name=record[2],
                        email=record[3],
                        username=record[4],
                    )
                    result.append(account)
                return result

    def get_account(self, username: str) -> AccountOutWithHashedPassword:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT *
                    FROM accounts
                    WHERE username = %s;
                    """,
                    [username],
                )
                try:
                    record = None
                    for row in cur.fetchall():
                        record = {}
                        for i, column in enumerate(cur.description):
                            record[column.name] = row[i]
                    return AccountOutWithHashedPassword(**record)
                except Exception:
                    return {"message": "Could not get account"}

    def create_account(
        self, data, hashed_password
    ) -> AccountOutWithHashedPassword:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    data.first_name,
                    data.last_name,
                    data.email,
                    data.username,
                    hashed_password,
                ]
                cur.execute(
                    """
                    INSERT INTO accounts (first_name, last_name, email, username, hashed_password)
                    VALUES (%s, %s, %s, %s, %s)
                    RETURNING id, first_name, last_name, email, username, hashed_password
                    """,
                    params,
                )
                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                return AccountOutWithHashedPassword(**record)

    def get():
        pass

    def update_account(
        self, account_id: int, data: Account
    ) -> Union[AccountOut, Error]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                params = [
                    data.first_name,
                    data.last_name,
                    data.email,
                    data.username,
                    account_id,
                ]
                db.execute(
                    """
                    UPDATE accounts
                    SET first_name = %s, last_name = %s, email = %s, username = %s
                    WHERE id = %s
                    RETURNING id, first_name, last_name, email, username
                    """,
                    params,
                )
                record = None
                row = db.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(db.description):
                        record[column.name] = row[i]
                return AccountOut(**record)

    def delete_account(self, account_id: int) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    DELETE FROM accounts
                    WHERE id = %s
                    """,
                    [account_id],
                )
                return True

    def get_one_account(self, account_id: int) -> AccountOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                        SELECT *
                        FROM accounts
                        WHERE id = %s
                    """,
                    [account_id],
                )
                record = cur.fetchone()
                account = AccountOut(
                    id=record[0],
                    first_name=record[1],
                    last_name=record[2],
                    email=record[3],
                    username=record[4],
                )
                return account
