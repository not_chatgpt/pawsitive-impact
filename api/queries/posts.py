from pydantic import BaseModel
from queries.pool import pool


class PostIn(BaseModel):
    pet_name: str
    species: str
    color: str
    age: str
    picture_url: str
    pet_size: str
    location_city: str
    location_state: str
    location_zipcode: int
    pet_description: str
    is_adopted: bool = False
    author_id: int
    interested: int = 0


class PostOut(PostIn):
    id: int


class PostRepo:
    def get_posts_from_account(self, author_id: int) -> list[PostOut]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                        SELECT *
                        FROM posts
                        WHERE author_id = %s
                        ORDER BY id;
                    """,
                    [author_id],
                )
                result = []
                for record in cur:
                    post = PostOut(
                        id=record[0],
                        pet_name=record[1],
                        species=record[2],
                        color=record[3],
                        age=record[4],
                        picture_url=record[5],
                        pet_size=record[6],
                        location_city=record[7],
                        location_state=record[8],
                        location_zipcode=record[9],
                        pet_description=record[10],
                        is_adopted=record[11],
                        author_id=record[12],
                        interested=record[13],
                    )
                    result.append(post)
                return result

    def get_one_post(self, post_id: int) -> PostOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                        SELECT *
                        FROM posts
                        WHERE id = %s
                    """,
                    [post_id],
                )
                record = cur.fetchone()
                post = PostOut(
                    id=record[0],
                    pet_name=record[1],
                    species=record[2],
                    color=record[3],
                    age=record[4],
                    picture_url=record[5],
                    pet_size=record[6],
                    location_city=record[7],
                    location_state=record[8],
                    location_zipcode=record[9],
                    pet_description=record[10],
                    is_adopted=record[11],
                    author_id=record[12],
                    interested=record[13],
                )
                return post

    def get_all(self) -> list[PostOut]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT *
                    FROM posts
                    ORDER BY id;
                    """,
                )
                result = []
                for record in cur:
                    post = PostOut(
                        id=record[0],
                        pet_name=record[1],
                        species=record[2],
                        color=record[3],
                        age=record[4],
                        picture_url=record[5],
                        pet_size=record[6],
                        location_city=record[7],
                        location_state=record[8],
                        location_zipcode=record[9],
                        pet_description=record[10],
                        is_adopted=record[11],
                        author_id=record[12],
                        interested=record[13],
                    )
                    result.append(post)
                return result

    def create_post(self, data: PostIn, author_id) -> PostOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    data.pet_name,
                    data.species,
                    data.color,
                    data.age,
                    data.picture_url,
                    data.pet_size,
                    data.location_city,
                    data.location_state,
                    data.location_zipcode,
                    data.pet_description,
                    data.is_adopted,
                    author_id,
                    data.interested,
                ]
                result = cur.execute(
                    """
                    INSERT INTO posts (pet_name, species, color, age, picture_url, pet_size, location_city, location_state, location_zipcode, pet_description, is_adopted, author_id, interested)
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                    RETURNING id, pet_name, species, color, age, picture_url, pet_size, location_city, location_state, location_zipcode, pet_description, is_adopted, author_id, interested
                    """,
                    params,
                )
                record = None
                row = result.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(result.description):
                        record[column.name] = row[i]
                return PostOut(**record)

    def update_post(self, post_id: int, data: PostIn, author_id) -> PostOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                params = [
                    data.pet_name,
                    data.species,
                    data.color,
                    data.age,
                    data.picture_url,
                    data.pet_size,
                    data.location_city,
                    data.location_state,
                    data.location_zipcode,
                    data.pet_description,
                    data.is_adopted,
                    author_id,
                    data.interested,
                    post_id,
                ]
                db.execute(
                    """
                    UPDATE posts

                    SET pet_name = %s,
                    species = %s,
                    color = %s,
                    age = %s,
                    picture_url = %s,
                    pet_size = %s,
                    location_city = %s,
                    location_state = %s,
                    location_zipcode = %s,
                    pet_description = %s,
                    is_adopted = %s,
                    author_id= %s,
                    interested= %s

                    WHERE id = %s

                    RETURNING id,
                    pet_name,
                    species,
                    color,
                    age,
                    picture_url,
                    pet_size,
                    location_city,
                    location_state,
                    location_zipcode,
                    pet_description,
                    is_adopted,
                    author_id,
                    interested
                    """,
                    params,
                )
                record = None
                row = db.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(db.description):
                        record[column.name] = row[i]
                return PostOut(**record)

    def delete_post(self, post_id: int) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    DELETE FROM posts
                    WHERE id = %s
                    """,
                    [post_id],
                )
                return True

    def update_adopted_field(self, post_id: int) -> PostOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                        UPDATE posts
                        SET is_adopted = True
                        WHERE id = %s
                        RETURNING id, pet_name, species, color, age, picture_url, pet_size, location_city, location_state, location_zipcode, pet_description, is_adopted, author_id, interested
                        """,
                    [post_id],
                )
                record = cur.fetchone()
                post = PostOut(
                    id=record[0],
                    pet_name=record[1],
                    species=record[2],
                    color=record[3],
                    age=record[4],
                    picture_url=record[5],
                    pet_size=record[6],
                    location_city=record[7],
                    location_state=record[8],
                    location_zipcode=record[9],
                    pet_description=record[10],
                    is_adopted=record[11],
                    author_id=record[12],
                    interested=record[13],
                )
                return post

    def get_all_adopted(self) -> list[PostOut]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT *
                    FROM posts
                    WHERE is_adopted = True
                    ORDER BY id;
                    """,
                )
                result = []
                for record in cur:
                    post = PostOut(
                        id=record[0],
                        pet_name=record[1],
                        species=record[2],
                        color=record[3],
                        age=record[4],
                        picture_url=record[5],
                        pet_size=record[6],
                        location_city=record[7],
                        location_state=record[8],
                        location_zipcode=record[9],
                        pet_description=record[10],
                        is_adopted=record[11],
                        author_id=record[12],
                    )
                    result.append(post)
                return result

    def get_all_not_adopted(self) -> list[PostOut]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT *
                    FROM posts
                    WHERE is_adopted = False
                    ORDER BY id;
                    """,
                )
                result = []
                for record in cur:
                    post = PostOut(
                        id=record[0],
                        pet_name=record[1],
                        species=record[2],
                        color=record[3],
                        age=record[4],
                        picture_url=record[5],
                        pet_size=record[6],
                        location_city=record[7],
                        location_state=record[8],
                        location_zipcode=record[9],
                        pet_description=record[10],
                        is_adopted=record[11],
                        author_id=record[12],
                    )
                    result.append(post)
                return result

    def update_interested_field(self, post_id: int, new_count: int) -> PostOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                        UPDATE posts
                        SET interested = %s
                        WHERE id = %s
                        RETURNING id, pet_name, species, color, age, picture_url, pet_size, location_city, location_state, location_zipcode, pet_description, is_adopted, author_id, interested
                        """,
                    [new_count, post_id],
                )
                record = cur.fetchone()
                post = PostOut(
                    id=record[0],
                    pet_name=record[1],
                    species=record[2],
                    color=record[3],
                    age=record[4],
                    picture_url=record[5],
                    pet_size=record[6],
                    location_city=record[7],
                    location_state=record[8],
                    location_zipcode=record[9],
                    pet_description=record[10],
                    is_adopted=record[11],
                    author_id=record[12],
                    interested=record[13],
                )
                return post
