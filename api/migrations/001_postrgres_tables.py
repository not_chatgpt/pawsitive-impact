steps = [
    [
        # "Up" SQL statement/ Create the table called accounts
        """
        CREATE TABLE accounts (
            id SERIAL PRIMARY KEY NOT NULL,
            first_name VARCHAR(1000) NOT NULL,
            last_name VARCHAR(1000) NOT NULL,
            email VARCHAR(1000)NOT NULL,
            username VARCHAR(1000)NOT NULL,
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE accounts;
        """
    ],
    # [
    #     # "Up" SQL statement/ Create the table called posts
    #     """
    #     CREATE TABLE posts (
    #         id SERIAL PRIMARY KEY NOT NULL,
    #         required_limited_text VARCHAR(1000) NOT NULL,
    #         required_unlimited_text TEXT NOT NULL,
    #         required_date_time TIMESTAMP NOT NULL,
    #         automatically_set_date_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    #         required_integer INTEGER NOT NULL,
    #         required_money MONEY NOT NULL
    #     );
    #     """,
    #     # "Down" SQL statement
    #     """
    #     DROP TABLE posts;
    #     """
    # ]
]
