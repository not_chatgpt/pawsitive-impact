from fastapi import Depends, APIRouter, HTTPException, UploadFile, File
from authenticator import authenticator
import tempfile
import os
import logging
from imgurpython import ImgurClient
import requests
from queries.posts import PostIn, PostOut, PostRepo

API_KEY_GOOGLE = os.environ.get("API_KEY_GOOGLE")
CLIENT_SECRET = os.environ.get("CLIENT_SECRET")
CLIENT_ID = os.environ.get("CLIENT_ID")


router = APIRouter()


@router.get("/api/posts", response_model=list[PostOut])
async def get_all_posts(
    repo: PostRepo = Depends(),
):
    return repo.get_all()


@router.post("/api/create_post", response_model=PostOut)
async def create_post(
    info: PostIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: PostRepo = Depends(),
) -> PostOut:
    id = account_data["id"]
    return repo.create_post(info, id)


@router.put("/api/posts/{post_id}", response_model=PostOut)
def update_post(
    post_id: int,
    info: PostIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: PostRepo = Depends(),
) -> PostOut:
    id = account_data["id"]
    return repo.update_post(post_id, info, id)


@router.delete("/api/posts/{post_id}", response_model=bool)
def delete_post(
    post_id: int,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: PostRepo = Depends(),
) -> bool:
    return repo.delete_post(post_id)


@router.put("/api/posts/{post_id}/adopted", response_model=PostOut)
def is_adopted(
    post_id: int,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: PostRepo = Depends(),
) -> PostOut:
    return repo.update_adopted_field(post_id)


@router.get("/api/posts/adopted", response_model=list[PostOut])
async def get_all_adopted_posts(
    repo: PostRepo = Depends(),
):
    return repo.get_all_adopted()


@router.get("/api/posts/not-adopted", response_model=list[PostOut])
async def get_all_not_adopted_posts(
    repo: PostRepo = Depends(),
):
    return repo.get_all_not_adopted()


@router.get("/api/posts/{post_id}", response_model=PostOut)
def get_post(
    post_id: int,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: PostRepo = Depends(),
) -> PostOut:
    return repo.get_one_post(post_id)


@router.put("/api/posts/{post_id}/interested", response_model=PostOut)
def is_interested(
    post_id: int,
    repo: PostRepo = Depends(),
) -> PostOut:
    current_post = repo.get_one_post(post_id)
    current_count = current_post.interested
    new_count = int(current_count) + 1
    return repo.update_interested_field(post_id, new_count)


@router.get("/api/posts/account", response_model=list[PostOut])
def get_posts_from_account(
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: PostRepo = Depends(),
):
    author_id = account_data["id"]
    return repo.get_posts_from_account(author_id)


@router.post("/api/upload-picture", response_model=dict)
async def upload_picture_to_imgur(file: UploadFile = File(...)):
    try:
        imgur_client = ImgurClient(CLIENT_ID, CLIENT_SECRET)
        contents = await file.read()
        with tempfile.NamedTemporaryFile(
            delete=False, suffix=file.filename
        ) as tmp:
            tmp.write(contents)
            temp_file_name = tmp.name

        logging.info("Uploading file to Imgur...")
        image = imgur_client.upload_from_path(temp_file_name)

        photo_id = image["id"]
        url = image["link"]

        return {"photo_id": photo_id, "url": url}
    except Exception as e:
        logging.error(f"An error occurred: {e}", exc_info=True)
        raise HTTPException(
            status_code=500, detail="An unexpected error occurred."
        )
    finally:
        logging.info("Removing temporary file...")
        os.remove(temp_file_name)

    # :)


@router.get("/list_shelters")
async def find_shelters(address: str, radius: int = 16000):
    def get_coordinates_from_address(address):
        geocode_url = f"https://maps.googleapis.com/maps/api/geocode/json?address={address}&key={API_KEY_GOOGLE}"
        response = requests.get(geocode_url)
        data = response.json()

        if data["status"] == "OK":
            latitude = data["results"][0]["geometry"]["location"]["lat"]
            longitude = data["results"][0]["geometry"]["location"]["lng"]
            locale = data["results"][0].get("locale", "").split("_")[0]
            return latitude, longitude, locale
        else:
            return None, None, None

    def fetch_shelter_data(latitude, longitude, radius, language):
        shelters = []
        nearby_search_url = f"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={latitude},{longitude}&radius={radius}&keyword=animal+shelter&key={API_KEY_GOOGLE}&language={language}"
        response = requests.get(nearby_search_url)
        data = response.json()

        if data["status"] == "OK":
            for place in data["results"]:
                shelter_info = {}
                shelter_info["Name"] = place["name"]
                shelter_info["Address"] = place.get("vicinity", "N/A")

                place_id = place["place_id"]
                place_details_url = f"https://maps.googleapis.com/maps/api/place/details/json?place_id={place_id}&fields=formatted_phone_number,website&key={API_KEY_GOOGLE}&language={language}"
                details_response = requests.get(place_details_url)
                details_data = details_response.json()

                if details_data["status"] == "OK":
                    phone_number = details_data["result"].get(
                        "formatted_phone_number", "N/A"
                    )
                    website = details_data["result"].get("website", "N/A")

                    shelter_info["Phone Number"] = phone_number
                    shelter_info["Website"] = website
                else:
                    shelter_info["Phone Number"] = "N/A"
                    shelter_info["Website"] = "N/A"

                shelters.append(shelter_info)
        return shelters

    latitude, longitude, language = get_coordinates_from_address(address)
    if latitude and longitude:
        return fetch_shelter_data(latitude, longitude, radius, language)
    else:
        raise HTTPException(status_code=400, detail="Could not fetch shelters")
