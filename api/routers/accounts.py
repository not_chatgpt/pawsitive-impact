from fastapi import (
    Depends,
    APIRouter,
    Request,
)
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator

from pydantic import BaseModel

from queries.accounts import (
    Account,
    AccountOutWithHashedPassword,
    AccountRepo,
    AccountOut,
    Error,
)

from typing import Union


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOutWithHashedPassword


class AccountTokenPartDeux(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.get("/api/accounts", response_model=list[AccountOut, Error])
async def get_all_accounts(
    repo: AccountRepo = Depends(),
):
    return repo.get_all()


@router.get("/token", response_model=AccountTokenPartDeux | None)
async def get_token(
    request: Request,
    account: Account = Depends(authenticator.try_get_current_account_data),
) -> AccountToken | None:
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }


@router.post("/api/accounts", response_model=Union[Account, Error])
async def create_account(
    info: Account,
    repo: AccountRepo = Depends(),
) -> Union[Account, Error]:
    hashed_password = authenticator.hash_password(info.password)
    return repo.create_account(info, hashed_password)


@router.put("/api/accounts", response_model=Union[AccountOut, Error])
def update_account(
    info: Account,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: AccountRepo = Depends(),
) -> Union[AccountOut, Error]:
    account_id = account_data["id"]
    return repo.update_account(account_id, info)


@router.delete("/api/accounts", response_model=bool)
def delete_account(
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: AccountRepo = Depends(),
) -> bool:
    account_id = account_data["id"]
    return repo.delete_account(account_id)


@router.get("/api/accounts", response_model=AccountOut)
def get_one_account(
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: AccountRepo = Depends(),
) -> Union[AccountOut, Error]:
    account_id = account_data["id"]
    return repo.get_one_account(account_id)


@router.get("/api/accounts/{account_id}", response_model=AccountOut)
def get_one_author(
    account_id=int,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: AccountRepo = Depends(),
) -> Union[AccountOut, Error]:
    return repo.get_one_account(account_id)
