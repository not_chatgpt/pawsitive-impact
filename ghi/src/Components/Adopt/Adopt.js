import React, { useEffect, useState } from "react";
import "./Adopt.css";
import { useNavigate } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
const REACT_APP_API_HOST = process.env.REACT_APP_API_HOST;


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: 'none',
    boxShadow: 24,
    p: 4,
    borderRadius: '15px',
};


function Adopt() {
    const [posts, setPosts] = useState([]);
    const navigate = useNavigate();
    const token = useToken()
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    const navigateToPostDetail = (postId) => {
        if (token.token) {
            navigate(`/main/adopt/${postId}/`);
        } else {
            handleOpen()
        }
    };

    useEffect(() => {
        const fetchData = async () => {
            const response = await fetch(
                `${REACT_APP_API_HOST}/api/posts/not-adopted/`
            );
            if (response.ok) {
                const data = await response.json();
                setPosts(data);
            }
        };

        fetchData();
    }, []);

    return (
        <>
            <span id="adoptTagline">A Home For Every Paw</span>
            <span id="adoptTagline2">Meet Our Adoptable Pets</span>
            <div className="container">
                {posts.map((post) => {
                    return (
                        <div key={post.id} onClick={() => navigateToPostDetail(post.id)} className="card__button" type="submit" value="adopt">
                            <div className="card">
                                <img src={post.picture_url} alt={post.pet_name} />
                                <div className="content">
                                    <h2 className="title">{post.pet_name}</h2>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>
            <div>
                <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style}>
                        <Typography id="modal-modal-title" variant="h6" component="h2">
                            You must be signed in to see!
                        </Typography>
                    </Box>
                </Modal>
            </div>
        </>
    );
}
export default Adopt;
