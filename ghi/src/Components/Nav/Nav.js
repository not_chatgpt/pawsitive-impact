import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import Container from "@mui/material/Container";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import Tooltip from "@mui/material/Tooltip";
import MenuItem from "@mui/material/MenuItem";
import PetsIcon from "@mui/icons-material/Pets";
import { Link } from "react-router-dom";
import { useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useEffect } from "react";


function Nav() {
  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);
  const [pages, setPages] = useState([])
  const [settings, setSettings] = useState([])
  const token = useToken()


  useEffect(() => {
    if (token.token) {
      let pages1 = [
        { label: "Success Stories", link: `main/adopt/success-stories`, key: 2 },
        { label: "Adopt", link: `/main/adopt`, key: 1 },
      ];
      const settings1 = [
        { label: "My Account", link: `/main/account-page/`, key: 3 },
        { label: "Find a Shelter", link: `/find-shelters`, key: 6 },
        { label: "Logout", link: `/logout`, key: 4 },
      ];
      setPages(pages1)
      setSettings(settings1)
    } else {
      let pages2 = [
        { label: "Adopt", link: `/main/adopt`, key: 10 },
        {label: "Success Stories", link: `main/adopt/success-stories`, key: 5},
        { label: "Find a Shelter", link: `/find-shelters`, key: 7 },
        { label: "Sign Up", link: "sign-up/", key: 8 },
        { label: "Login", link: "login/", key: 9 },
      ];
      setPages(pages2)
      setSettings([])
    }
  }, [token.token]);


  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  return (
    <div className="navcontainer" style={{ zIndex: '-1' }}>
      <AppBar position="static" style={{ background: '#E1B382', boxShadow: 'none', zIndex: '2' }}>
        <Container maxWidth="xl">
          <Toolbar disableGutters>
            <PetsIcon style={{ zIndex: '100' }}
              sx={{
                display: { xs: "none", md: "flex" },
                mr: 1,
                textShadow: "0 0 20px #000000",
              }}
            />
            <Typography
              style={{ zIndex: '100' }}
              variant="h6"
              noWrap
              component="a"
              href="/"
              sx={{
                mr: 2,
                display: { xs: "none", md: "flex" },
                fontFamily: "monospace",
                fontWeight: 700,
                letterSpacing: ".3rem",
                color: "inherit",
                textDecoration: "none",
                textShadow: "0 0 10px #000000",
              }}
            >
              Pawsitive Impact
            </Typography>

            <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
              <IconButton style={{ zIndex: '100' }}
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleOpenNavMenu}
                color="inherit"
              >
                <MenuIcon />
              </IconButton>
              <Menu
                style={{ zIndex: '100' }}
                id="menu-appbar"
                anchorEl={anchorElNav}
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "left",
                }}
                keepMounted
                transformOrigin={{
                  vertical: "top",
                  horizontal: "left",
                }}
                open={Boolean(anchorElNav)}
                onClose={handleCloseNavMenu}
                sx={{
                  display: { xs: "block", md: "none" },
                }}
              >
                {pages.map((page) => (
                  <MenuItem key={page.key} onClick={handleCloseNavMenu}>
                    <Link to={page.link} style={{ textDecoration: "none" }}>
                      <Typography textAlign="center" sx={{ textShadow: "0 0 10px #000000" }}>{page.label}</Typography>
                    </Link>
                  </MenuItem>
                ))}
              </Menu>
            </Box>
            <PetsIcon style={{ zIndex: '100' }} sx={{ display: { xs: "flex", md: "none" }, mr: 1 }} />
            <Typography
              style={{ zIndex: '100' }}
              variant="h5"
              noWrap
              component="a"
              href="/"
              sx={{
                mr: 2,
                display: { xs: "flex", md: "none" },
                flexGrow: 1,
                fontFamily: "monospace",
                fontWeight: 700,
                letterSpacing: ".3rem",
                color: "inherit",
                textDecoration: "none",
                textShadow: "0 0 10px #000000",
              }}
            >
              Pawsitive Impact
            </Typography>
            <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
              {pages.map((page) => (
                <Link to={page.link} style={{ textDecoration: "none" }} key={page.key}>
                  <Button style={{ zIndex: '100' }}
                    onClick={handleCloseNavMenu}
                    sx={{ textShadow: "0 0 10px #000000", my: 2, color: "white", display: "block" }}
                  >
                    {page.label}
                  </Button>
                </Link>
              ))}
            </Box>

            <Box sx={{ flexGrow: 0 }}>
              <Tooltip title="Open settings">
                <IconButton style={{ zIndex: '100' }} onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                  <Avatar alt="Remy Sharp" src="" />
                </IconButton>
              </Tooltip>
              <Menu
                sx={{ mt: "45px" }}
                id="menu-appbar"
                anchorEl={anchorElUser}
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                keepMounted
                transformOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                open={Boolean(anchorElUser)}
                onClose={handleCloseUserMenu}
              >
                {settings.map((setting) => (
                  <MenuItem key={setting.key} onClick={handleCloseUserMenu}>
                    <Link to={setting.link} style={{ textDecoration: "none" }}>
                      <Typography textAlign="center">{setting.label}</Typography>
                    </Link>
                  </MenuItem>
                ))}
              </Menu>
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
    </div>
  );
}
export default Nav;
