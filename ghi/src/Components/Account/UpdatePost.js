import React, { useEffect, useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate, useParams } from "react-router-dom";
import TextField from '@mui/material/TextField';
import './UpdatePost.css'

export default function UpdatePost() {
  const [image, setImage] = useState(null);
  const [imageUrl, setImageUrl] = useState("");
  const [setPostInfo] = useState({});
  const navigate = useNavigate();
  const { token } = useToken();
  const { post_id } = useParams();
  const REACT_APP_API_HOST = process.env.REACT_APP_API_HOST;

  const [formData, setFormData] = useState({
    pet_name: "",
    species: "",
    color: "",
    age: "",
    pet_size: "",
    location_city: "",
    location_state: "",
    location_zipcode: 0,
    pet_description: "",
    is_adopted: false,
    author_id: 0,
    interested: 0,
    picture_url: "",
  });

  const [isUpdating, setIsUpdating] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      const url = `${REACT_APP_API_HOST}/api/posts/${post_id}`;
      const response = await fetch(url, {
        method: "GET",
        headers: { Authorization: `Bearer ${token}` },
      });

      if (response.ok) {
        const data = await response.json();
        setPostInfo(data);

        setFormData({
          pet_name: data.pet_name,
          species: data.species,
          color: data.color,
          age: data.age,
          pet_size: data.pet_size,
          location_city: data.location_city,
          location_state: data.location_state,
          location_zipcode: data.location_zipcode,
          pet_description: data.pet_description,
          is_adopted: data.is_adopted,
          author_id: data.author_id,
          interested: data.interested,
          picture_url: data.picture_url,
        });
      }
    };
    fetchData();
  }, [post_id, token, REACT_APP_API_HOST, setPostInfo]);

  const handleImageChange = (e) => {
    const file = e.target.files[0];
    const reader = new FileReader();

    const allowedFormats = ["image/jpeg", "image/bmp", "image/png"];
    if (file && !allowedFormats.includes(file.type)) {
      alert("Image format must be JPG, BMP, or PNG");
      return;
    }

    const maxSizeInBytes = 20 * 1024 * 1024;
    if (file && file.size > maxSizeInBytes) {
      alert("Image size must be less than 20MB");
      return;
    }

    reader.onload = () => {
      setImage(file);
      setImageUrl(reader.result);
    };
    reader.readAsDataURL(file);
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    setIsUpdating(true);

    if (image) {
      const imageFormData = new FormData();
      imageFormData.append("file", image);

      try {
        const imageResponse = await fetch(
          `${REACT_APP_API_HOST}/api/upload-picture`,
          {
            method: "POST",
            body: imageFormData,
          }
        );

        if (!imageResponse.ok) {
          console.error("Image upload failed");
          const errorText = await imageResponse.text();
          console.error("Image upload error details:", errorText);
          setIsUpdating(false);
          return;
        }

        const imageData = await imageResponse.json();
        const updatedFormData = {
          ...formData,
          picture_url: imageData.url,
        };

        try {
          const postResponse = await fetch(
            `${REACT_APP_API_HOST}/api/posts/${post_id}`,
            {
              method: "PUT",
              headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
              },
              body: JSON.stringify(updatedFormData),
            }
          );

          if (postResponse.ok) {
            console.log("Post updated successfully!");
            navigate(`/main/account-page/`);
          } else {
            console.error("Failed to update the post.");
          }
        } catch (error) {
          console.error("An error occurred:", error);
        }
      } catch (error) {
        console.error("An error occurred:", error);
      } finally {
        setIsUpdating(false);
      }
    } else {
      try {
        const postResponse = await fetch(
          `${REACT_APP_API_HOST}/api/posts/${post_id}`,
          {
            method: "PUT",
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${token}`,
            },
            body: JSON.stringify(formData),
          }
        );

        if (postResponse.ok) {
          console.log("Post updated successfully!");
          navigate(`/main/account-page/`);
        } else {
          console.error("Failed to update the post.");
        }
      } catch (error) {
        console.error("An error occurred:", error);
      } finally {
        setIsUpdating(false);
      }
    }
  };


  return (
    <>
    <div className="container">
      <h2 className="updatepostHeader">Update Your Pet Information</h2>
      <div className="updatePost">
        <div className="left-container">
          <div className="puppy slide-in-bottom">
              <img src="https://i.imgur.com/1sN75xk.png" alt="bunny"/>
          </div>
        </div>
        <div className="right-container">
          <div className="update-post-form"></div>
            <form onSubmit={handleSubmit} id="update-post-form">
              <div className="form-group">
                <label htmlFor="file">Upload Image</label>
                <input
                  type="file"
                  accept="image/*"
                  onChange={handleImageChange}
                  className="form-control"
                />
                {imageUrl && (
                  <img
                    src={imageUrl}
                    alt="Preview"
                    className="img-thumbnail"
                    style={{ maxWidth: "200px" }}
                  />
                )}
              </div>
              <div className="form-group">
                <TextField
                  type="text"
                  name="pet_name"
                  value={formData.pet_name}
                  onChange={handleInputChange}
                  label="Pet Name"
                  variant="standard"
                  className="form-control"
                  style={{ backgroundColor: "#f7b1ab" }}
                />
              </div>
              <div className="form-group">
                <TextField
                  type="text"
                  name="species"
                  value={formData.species}
                  onChange={handleInputChange}
                  label="Species"
                  variant="standard"
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <TextField
                  type="text"
                  name="color"
                  value={formData.color}
                  onChange={handleInputChange}
                  label="Color"
                  variant="standard"
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <TextField
                  type="text"
                  name="age"
                  value={formData.age}
                  onChange={handleInputChange}
                  label="Age"
                  variant="standard"
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <TextField
                  type="text"
                  name="pet_size"
                  value={formData.pet_size}
                  onChange={handleInputChange}
                  label="Pet Size"
                  variant="standard"
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <TextField
                  type="text"
                  name="location_city"
                  value={formData.location_city}
                  onChange={handleInputChange}
                  label="City"
                  variant="standard"
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <TextField
                  type="text"
                  name="location_state"
                  value={formData.location_state}
                  onChange={handleInputChange}
                  label="State"
                  variant="standard"
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <TextField
                  type="text"
                  name="location_zipcode"
                  value={formData.location_zipcode}
                  onChange={handleInputChange}
                  label="Zipcode"
                  variant="standard"
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <TextField
                  name="pet_description"
                  value={formData.pet_description}
                  onChange={handleInputChange}
                  label="Pet Description"
                  multiline
                  rows={4}
                  variant="standard"
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <button
                  type="submit" className="update-post-btn type--A" disabled={isUpdating} >
                  <div className="update-post-btn__line"></div>
                  <div className="update-post-btn__line"></div>
                  <span className="update-post-btn__text">Update</span>
                  <div className="update-post-btn-content">
                  { isUpdating ? (
                    <div className="update-post-btn-loading">
                      <i
                        className="fas fa-spinner fa-spin"
                        style={{ fontSize: "18px", marginRight: "8px" }}
                      />
                      Updating post... please wait!
                    </div>
                  ) : (
                    " "
                  )}
                  </div>
                  <div className="update-post-btn__drow1"></div>
                  <div className="update-post-btn__drow2"></div>
                </button>
              </div>
            </form>
          </div>
      </div>
    </div>
    </>
  );
}
