import React, { useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";
import './PostForm.css'
import TextField from '@mui/material/TextField';

const REACT_APP_API_HOST = process.env.REACT_APP_API_HOST;



const PostForm = () => {
  const navigate = useNavigate();
  const [image, setImage] = useState(null);
  const [imageUrl, setImageUrl] = useState("");
  const [formData, setFormData] = useState({
    pet_name: "",
    species: "",
    color: "",
    age: "",
    pet_size: "",
    location_city: "",
    location_state: "",
    location_zipcode: 0,
    pet_description: "",
    is_adopted: false,
    author_id: 0,
    interested: 0,
  });

  const { token } = useToken();
  const [loading, setLoading] = useState(false);


  const handleImageChange = (e) => {
    const file = e.target.files[0];
    const reader = new FileReader();
    const allowedFormats = ["image/jpeg", "image/bmp", "image/png"];
    if (file && !allowedFormats.includes(file.type)) {
      alert("Image format must be JPG, BMP, or PNG");
      return;
    }

    const maxSizeInBytes = 20 * 1024 * 1024;
    if (file && file.size > maxSizeInBytes) {
      alert("Image size must be less than 20MB");
      return;
    }

    reader.onload = () => {
      setImage(file);
      setImageUrl(reader.result);
    };
    reader.readAsDataURL(file);
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const imageFormData = new FormData();
    imageFormData.append("file", image);

    setLoading(true);

    try {
      const imageResponse = await fetch(
        `${REACT_APP_API_HOST}/api/upload-picture`,
        {
          method: "POST",
          body: imageFormData,
        }
      );

      if (!imageResponse.ok) {
        console.error("Image upload failed");
        setLoading(false); // Reset loading state
        return;
      }

      const imageData = await imageResponse.json();
      const updatedFormData = {
        ...formData,
        picture_url: imageData.url,
      };

      const postResponse = await fetch(
        `${REACT_APP_API_HOST}/api/create_post`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify(updatedFormData),
        }
      );

      if (postResponse.ok) {
        navigate(`/main/account-page/`);
      } else {
        console.error("Failed to create the post.");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    } finally {
      setLoading(false);
    }
  };

  const handleCancel = async () => {
      navigate("/main/account-page/")
  }

  return (
    <>
      <div className="container">
        <h2 className="createpostHeader">Let's help find your pet's perfect new home</h2>
        <div className="createPost">
          <div className="left-container">

            <div className="puppy slide-in-bottom">
              <img src="https://i.imgur.com/5fcUR5p.png" alt= "puppic"/>
            </div>
          </div>
          <div className="right-container">
            <div className="create-post-form">
              <form onSubmit={handleSubmit} id="create-post-form">
                <div className="form-group">
                  <label htmlFor="file">Upload An Image</label>
                  <input
                    type="file"
                    accept="image/*"
                    onChange={handleImageChange}
                    className="form-control"
                  />
                  {imageUrl && (
                    <img
                      src={imageUrl}
                      alt="Preview"
                      className="img-thumbnail"
                      style={{ maxWidth: "200px" }}
                    />
                  )}
                </div>
                <div className="form-group">
                  <TextField
                    type="text"
                    name="pet_name"
                    value={formData.pet_name}
                    onChange={handleInputChange}
                    label="Pet Name"
                    variant="standard"
                    className="form-control"
                    style={{ backgroundColor: "#f7b1ab" }}
                  />
                </div>
                <div className="form-group">
                  <TextField
                    type="text"
                    name="species"
                    value={formData.species}
                    onChange={handleInputChange}
                    label="Species"
                    variant="standard"
                    className="form-control"
                  />
                </div>
                <div className="form-group">
                  <TextField
                    type="text"
                    name="color"
                    value={formData.color}
                    onChange={handleInputChange}
                    label="Color"
                    variant="standard"
                    className="form-control"
                  />
                </div>
                <div className="form-group">
                  <TextField
                    type="text"
                    name="age"
                    value={formData.age}
                    onChange={handleInputChange}
                    label="Age"
                    variant="standard"
                    className="form-control"
                  />
                </div>
                <div className="form-group">
                  <TextField
                    type="text"
                    name="pet_size"
                    value={formData.pet_size}
                    onChange={handleInputChange}
                    label="Pet Size"
                    variant="standard"
                    className="form-control"
                  />
                </div>
                <div className="form-group">
                  <TextField
                    type="text"
                    name="location_city"
                    value={formData.location_city}
                    onChange={handleInputChange}
                    label="City"
                    variant="standard"
                    className="form-control"
                  />
                </div>
                <div className="form-group">
                  <TextField
                    type="text"
                    name="location_state"
                    value={formData.location_state}
                    onChange={handleInputChange}
                    label="State"
                    variant="standard"
                    className="form-control"
                  />
                </div>
                <div className="form-group">
                  <TextField
                    type="text"
                    name="location_zipcode"
                    value={formData.location_zipcode}
                    onChange={handleInputChange}
                    label="Zipcode"
                    variant="standard"
                    className="form-control"
                  />
                </div>
                <div className="form-group">
                  <TextField
                    type="text"
                    name="pet_description"
                    value={formData.pet_description}
                    onChange={handleInputChange}
                    label="Pet Description"
                    multiline
                    rows={4}
                    variant="standard"
                    className="form-control"
                  />
                </div>
                <button
                  type="submit" className="post-form-btn type--A" disabled={loading}>
                  <div class="post-form-btn__line"></div>
                  <div class="post-form-btn__line"></div>
                  <span className="post-form-btn__text">Create</span>
                  <div className="post-form-btn-content">
                    {loading ? (
                        <div className="post-form-btn-loading">
                        <i
                          className="fas fa-spinner fa-spin"
                          style={{ fontSize: "18px", marginRight: "8px" }}
                        />
                        Creating post... please wait!
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                  <div class="post-form-btn__drow1"></div>
                  <div class="post-form-btn__drow2"></div>
                </button>
                  <button
                  onClick={handleCancel}
                  type="submit" className="post-form-btn type--A" disabled={loading}>
                  <div class="post-form-btn__line"></div>
                  <div class="post-form-btn__line"></div>
                  <span className="post-form-btn__text">Cancel</span>
                  <div className="post-form-btn-content">
                    {loading ? (
                        <div className="post-form-btn-loading">
                        <i
                          className="fas fa-spinner fa-spin"
                          style={{ fontSize: "18px", marginRight: "8px" }}
                        />
                        Canceling post... please wait!
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                  <div class="post-form-btn__drow1"></div>
                  <div class="post-form-btn__drow2"></div>
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default PostForm;


// import React, { useState } from "react";
// import useToken from "@galvanize-inc/jwtdown-for-react";
// import { useNavigate } from "react-router-dom";
// import "./PostForm.css";
// import TextField from "@mui/material/TextField";


// const REACT_APP_API_HOST = process.env.REACT_APP_API_HOST;


// function ImageUpload({ onImageUrlChange }) {
//  const [image, setImage] = useState(null);
//  const [previewImageUrl, setPreviewImageUrl] = useState(null);


//  const handleImageUpload = async (e) => {
//    const file = e.target.files[0];


//    if (!isAcceptedFormat(file.type)) {
//      alert("Image format must be JPG, BMP, PNG, GIF, TIFF, or WEBP");
//      return;
//    }


//    setPreviewImageUrl(URL.createObjectURL(file));


//    const formData = new FormData();
//    formData.append("key", "1fce1cb124450fb3b85211adab05e246");
//    formData.append("image", file);


//    try {
//      const response = await fetch("https://api.imgbb.com/1/upload", {
//        method: "POST",
//        body: formData,
//      });


//      if (response.ok) {
//        const data = await response.json();
//        const uploadedImageUrl = data.data.url;
//        onImageUrlChange(uploadedImageUrl);
//      } else {
//        console.error("Failed to upload the image.");
//      }
//    } catch (error) {
//      console.error("An error occurred:", error);
//    }
//  };


//  const isAcceptedFormat = (fileType) => {
//    const acceptedFormats = [
//      "image/jpeg",
//      "image/bmp",
//      "image/png",
//      "image/gif",
//      "image/tiff",
//      "image/webp",
//    ];
//    return acceptedFormats.includes(fileType);
//  };


//  return (
//    <div>
//      <input type="file" accept="image/*" onChange={handleImageUpload} />
//      {previewImageUrl && (
//        <div>
//          <img
//            src={previewImageUrl}
//            alt="Image Preview"
//            style={{ maxWidth: "200px" }}
//          />
//        </div>
//      )}
//    </div>
//  );
// }


// const PostForm = () => {
//  const navigate = useNavigate();
//  const [formData, setFormData] = useState({
//    pet_name: "",
//    species: "",
//    color: "",
//    age: "",
//    pet_size: "",
//    location_city: "",
//    location_state: "",
//    location_zipcode: 0,
//    pet_description: "",
//    is_adopted: false,
//    author_id: 0,
//    interested: 0,
//  });


//  const { token } = useToken();
//  const [loading, setLoading] = useState(false);
//  const [imageUrl, setImageUrl] = useState("");


//  const handleInputChange = (e) => {
//    const { name, value } = e.target;
//    setFormData({ ...formData, [name]: value });
//  };


//  const handleImageUrlChange = (url) => {
//    setImageUrl(url);
//  };


//  const handleSubmit = async (e) => {
//    e.preventDefault();


//    const updatedFormData = {
//      ...formData,
//      picture_url: imageUrl,
//    };


//    setLoading(true);


//    try {
//      const postResponse = await fetch(
//        `${REACT_APP_API_HOST}/api/create_post`,
//        {
//          method: "POST",
//          headers: {
//            "Content-Type": "application/json",
//            Authorization: `Bearer ${token}`,
//          },
//          body: JSON.stringify(updatedFormData),
//        }
//      );


//      if (postResponse.ok) {
//        navigate(`/main/account-page/`);
//      } else {
//        console.error("Failed to create the post.");
//      }
//    } catch (error) {
//      console.error("An error occurred:", error);
//    } finally {
//      setLoading(false);
//    }
//  };


//  return (
//    <div className="container">
//      <h2 className="createpostHeader">
//        Let's help find your pet's perfect new home
//      </h2>
//      <div className="createPost">
//        <div className="left-container">
//          <div className="puppy slide-in-bottom">
//            <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/38816/image-from-rawpixel-id-542207-jpeg.png" />
//          </div>
//        </div>
//        <div className="right-container">
//          <div className="create-post-form">
//            <form onSubmit={handleSubmit} id="create-post-form">
//              <div className="form-group">
//                <label htmlFor="file">Upload An Image</label>
//                <ImageUpload onImageUrlChange={handleImageUrlChange} />
//              </div>
//              <div className="form-group">
//                <TextField
//                  type="text"
//                  name="pet_name"
//                  value={formData.pet_name}
//                  onChange={handleInputChange}
//                  label="Pet Name"
//                  variant="standard"
//                  className="form-control"
//                  style={{ backgroundColor: "#f7b1ab" }}
//                />
//              </div>
//              <div className="form-group">
//                <TextField
//                  type="text"
//                  name="species"
//                  value={formData.species}
//                  onChange={handleInputChange}
//                  label="Species"
//                  variant="standard"
//                  className="form-control"
//                />
//              </div>
//              <div className="form-group">
//                <TextField
//                  type="text"
//                  name="color"
//                  value={formData.color}
//                  onChange={handleInputChange}
//                  label="Color"
//                  variant="standard"
//                  className="form-control"
//                />
//              </div>
//              <div className="form-group">
//                <TextField
//                  type="text"
//                  name="age"
//                  value={formData.age}
//                  onChange={handleInputChange}
//                  label="Age"
//                  variant="standard"
//                  className="form-control"
//                />
//              </div>
//              <div className="form-group">
//                <TextField
//                  type="text"
//                  name="pet_size"
//                  value={formData.pet_size}
//                  onChange={handleInputChange}
//                  label="Pet Size"
//                  variant="standard"
//                  className="form-control"
//                />
//              </div>
//              <div className="form-group">
//                <TextField
//                  type="text"
//                  name="location_city"
//                  value={formData.location_city}
//                  onChange={handleInputChange}
//                  label="City"
//                  variant="standard"
//                  className="form-control"
//                />
//              </div>
//              <div className="form-group">
//                <TextField
//                  type="text"
//                  name="location_state"
//                  value={formData.location_state}
//                  onChange={handleInputChange}
//                  label="State"
//                  variant="standard"
//                  className="form-control"
//                />
//              </div>
//              <div className="form-group">
//                <TextField
//                  type="text"
//                  name="location_zipcode"
//                  value={formData.location_zipcode}
//                  onChange={handleInputChange}
//                  label="Zipcode"
//                  variant="standard"
//                  className="form-control"
//                />
//              </div>
//              <div className="form-group">
//                <TextField
//                  type="text"
//                  name="pet_description"
//                  value={formData.pet_description}
//                  onChange={handleInputChange}
//                  label="Pet Description"
//                  multiline
//                  rows={4}
//                  variant="standard"
//                  className="form-control"
//                />
//              </div>
//              <button
//                type="submit"
//                className="button type--A"
//                disabled={loading}
//              >
//                <div className="button__line"></div>
//                <div className="button__line"></div>
//                {loading ? (
//                  <>
//                    <i
//                      className="fas fa-spinner fa-spin"
//                      style={{ fontSize: "18px", marginRight: "8px" }}
//                    />
//                    Creating post... please wait!
//                  </>
//                ) : (
//                  "Create Post"
//                )}
//                <div className="button__drow1"></div>
//                <div className="button__drow2"></div>
//              </button>
//            </form>
//          </div>
//        </div>
//      </div>
//    </div>
//  );
// };


// export default PostForm;