import useToken from "@galvanize-inc/jwtdown-for-react";
import { useEffect, useState } from "react";
import TextField from '@mui/material/TextField';
import './UpdateAccount.css'
import { useNavigate } from "react-router-dom";
const REACT_APP_API_HOST = process.env.REACT_APP_API_HOST;

export default function UpdateAccount() {
    const [setCurrentAccount] = useState({
    first_name: "",
    last_name: "",
    email: "",
    username: "",
    });

    const [first_name, setFirstName] = useState("");
    const [last_name, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [checkpassword, setCheckPassword] = useState("");
    const [isUpdating] = useState(false);

    const { token } = useToken();
    const navigate = useNavigate();

    const handleFirstNameChange = (event) => {
    const value = event.target.value;
    setFirstName(value);
    };

    const handleLastNameChange = (event) => {
    const value = event.target.value;
    setLastName(value);
    };

    const handleEmailChange = (event) => {
    const value = event.target.value;
    setEmail(value);
    };

    const handleUsernameChange = (event) => {
    const value = event.target.value;
    setUsername(value);
    };

    const handlePasswordChange = (event) => {
    const value = event.target.value;
    setPassword(value);
    };

    const handleCheckPasswordChange = (event) => {
    const value = event.target.value;
    setCheckPassword(value);
    };

    useEffect(() => {
    const fetchData = async () => {
        const url = `${REACT_APP_API_HOST}/api/accounts/`;
        const response = await fetch(url, {
        method: "GET",
        headers: { Authorization: `Bearer ${token}` },
        });

        if (response.ok) {
        const data = await response.json();
        setCurrentAccount(data);

        setFirstName(data.first_name);
        setLastName(data.last_name);
        setEmail(data.email);
        setUsername(data.username);
        }
    };
    fetchData();
    }, [token, setCurrentAccount]);

    const handleCancel = async () => {
        navigate("/main/account-page/")
    }

    const handleSubmit = async (event) => {
    event.preventDefault();
    const userData = {
        first_name: first_name,
        last_name: last_name,
        email: email,
        username: username,
        password: password,
    };

    if (checkpassword === password) {
        const url = `${REACT_APP_API_HOST}/api/accounts/`;
        await fetch(url, {
        method: "PUT",
        body: JSON.stringify(userData),
        headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
        },
        });
        navigate(`/main/account-page/`);
    } else {
        console.error("passwords did not match");
    }
    };

    return (
    <>
        <div className="container">
            <h2 className="updateaccountHeader">Update Your Account!</h2>
            <div className="updateAccount">
                <div className="left-container">
                    <div className="puppy slide-in-bottom">
                            <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/38816/image-from-rawpixel-id-542207-jpeg.png" alt="doggopic"/>
                    </div>
                </div>
                <div className="right-container">
                    <div className="update-account-form"></div>
                    <form onSubmit={handleSubmit} id="update-account-forms">
                        <div>
                        <TextField
                            onChange={handleFirstNameChange}
                            value={first_name}
                            required
                            type="text"
                            name="first_name"
                            id="first_name"
                            className="form-control"
                            label="First Name"
                            variant="standard"
                        />
                        </div>
                        <div>
                        <TextField
                            onChange={handleLastNameChange}
                            value={last_name}
                            required
                            type="text"
                            name="last_name"
                            id="last_name"
                            className="form-control"
                            label="Last Name"
                            variant="standard"
                        />
                        </div>
                        <div>
                        <TextField
                            onChange={handleEmailChange}
                            value={email}
                            required
                            type="email"
                            name="email"
                            id="email"
                            className="form-control"
                            label="email"
                            variant="standard"
                        />
                        </div>
                        <div>
                        <TextField
                            onChange={handleUsernameChange}
                            value={username}
                            required
                            type="text"
                            name="username"
                            id="username"
                            className="form-control"
                            label="username"
                            variant="standard"
                        />
                        </div>
                        <div>
                        <TextField
                            onChange={handlePasswordChange}
                            value={password}
                            required
                            type="password"
                            name="password"
                            id="password"
                            className="form-control"
                            label="password"
                            variant="standard"
                        />
                        </div>
                        <div>
                        <TextField
                            onChange={handleCheckPasswordChange}
                            value={checkpassword}
                            required
                            type="password"
                            name="check_password"
                            id="check_password"
                            className="form-control"
                            label="check password"
                            variant="standard"
                        />
                        </div>
                            <button
                                type="submit" className="update-account-btn type--A" disabled={isUpdating} >
                                <div className="update-account-btn__line"></div>
                                <div className="update-account-btn__line"></div>
                                <span className="update-account-btn__text">Update</span>
                                <div className="update-account-btn-content">
                                {isUpdating ? (
                                    <div className="update-account-btn-loading">
                                        <i
                                            className="fas fa-spinner fa-spin"
                                            style={{ fontSize: "18px", marginRight: "8px" }}
                                        />
                                        Updating post... please wait!
                                    </div>
                                ) : (
                                    ""
                                )}
                                </div>
                                <div className="update-account-btn__drow1"></div>
                                <div className="update-account-btn__drow2"></div>
                            </button>
                            <button
                                onClick={handleCancel}
                                type="submit" className="update-account-btn type--A" disabled={isUpdating} >
                                <div className="update-account-btn__line"></div>
                                <div className="update-account-btn__line"></div>
                                <span className="update-account-btn__text">Cancel</span>
                                <div className="update-account-btn-content">
                                {isUpdating ? (
                                    <div className="update-account-btn-loading">
                                        <i
                                            className="fas fa-spinner fa-spin"
                                            style={{ fontSize: "18px", marginRight: "8px" }}
                                        />
                                        Canceling post... please wait!
                                    </div>
                                ) : (
                                    ""
                                )}
                                </div>
                                <div className="update-account-btn__drow1"></div>
                                <div className="update-account-btn__drow2"></div>
                            </button>
                    </form>
                </div>
            </div>
        </div>
    </>
    );
}
