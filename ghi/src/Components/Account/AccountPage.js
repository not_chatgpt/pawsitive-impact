import useToken from "@galvanize-inc/jwtdown-for-react";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import * as React from 'react';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import "./AccountPageCSS.css";


const REACT_APP_API_HOST = process.env.REACT_APP_API_HOST;


const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  boxShadow: 15,
  p: 4,
  borderRadius: 10,
};


export default function AccountPage() {
    const { token } = useToken();
    const [posts, setPosts] = useState([])
    const [account, setAccount] = useState([])
    const navigate = useNavigate()
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    useEffect(() => {
        const fetchdata = async () => {
            const url = `${REACT_APP_API_HOST}/api/posts/account/`
            const response = await fetch(url, {
                "method": "GET",
                headers: { Authorization: `Bearer ${token}` }
            })

            if (response.ok) {
                const data = await response.json()
                setPosts(data)
            }
        }
        fetchdata()
    }, [token]);

    useEffect(() => {
        const fetchAccountData = async () => {
            const url = `${REACT_APP_API_HOST}/api/accounts/`
            const response = await fetch(url, {
                "method": "GET",
                headers: { Authorization: `Bearer ${token}` }
            })

            if (response.ok) {
                const data = await response.json()
                setAccount(data)
            }
        }
        fetchAccountData()
    }, [token]);


    async function handleEditRequest(post) {
        let post_id = post.id
        navigate(`update-post/${post_id}`)
    }

    async function handleDeleteRequest(post) {
        let post_id = post.id
        const url = `${REACT_APP_API_HOST}/api/posts/${post_id}`
        await fetch(url, {
            "method": "DELETE",
            headers: { Authorization: `Bearer ${token}` }
        })
        window.location.reload(false)
    }

    async function handleAdoptedRequest(post) {
        let post_id = post.id
        const url = `${REACT_APP_API_HOST}/api/posts/${post_id}/adopted`
        await fetch(url, {
            "method": "PUT",
            headers: { Authorization: `Bearer ${token}` }
        })
        handleOpen()
    }
    const handleCreatePost = async () => {
        navigate(`post-form/`)
    }

    const handleUpdateRequest = async () => {
        navigate(`update-account`)
    }

    return (
        <>
            <div id='main-grid'>
                <div class='account-box'>
                    <Typography class='account-details-header' gutterBottom>
                        Your Account Details:
                    </Typography>
                    <Typography class='account-details'>
                        {account.first_name} {account.last_name}
                    </Typography>
                    <Typography class='account-details'>
                        Your contact email is: {account.email}
                    </Typography>
                    <Typography class='account-details'>
                        Your username is: {account.username}
                        <br />
                    </Typography>
                    <Typography class='account-details'>
                        <button class='account-buttons' onClick={handleUpdateRequest}>Update Account</button>
                    </Typography>
                </div>
                <Button class='account-create-buttons' onClick={handleCreatePost}>Create a post</Button>
                <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style}>
                        <Typography id="modal-modal-title" variant="h6" component="h2">
                            Yay! You're animal was adopted!
                        </Typography>
                        <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                            We will be displaying this post in our success story section! Head over there and give it a look.
                        </Typography>
                        <Typography id="modal-modal-description" sx={{ mt: 3 }}>
                            We would like to thank you again for choosing to adopt!
                        </Typography>
                        <Typography id="modal-modal-description" sx={{ mt: 3 }}>
                            Click outside of this box to return to your account page.
                        </Typography>
                    </Box>
                </Modal>
                <h1 className='header-posts'>Your Adoption Posts</h1>
            </div>
            <div className="container">
                {posts.map((post) => {
                    return (
                        <div key={post.id} className="card">
                            <img src={post.picture_url} alt={post.pet_name} />
                            <div className="content">
                                <div id="post-name">{post.pet_name}</div>
                                <Button id='post-buttons' onClick={() => handleEditRequest(post)} size="small">Edit</Button>
                                <Button id='post-buttons' onClick={() => handleDeleteRequest(post)} size="small">Delete</Button>
                                <Button id='post-buttons' onClick={() => handleAdoptedRequest(post)} size="small">Adopted?</Button>
                            </div>
                        </div>
                    );
                })}
            </div>
        </>
    )
}
