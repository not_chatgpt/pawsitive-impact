import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import './PostDetails.css'
const REACT_APP_API_HOST = process.env.REACT_APP_API_HOST;

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: 'none',
  boxShadow: 24,
  p: 4,
  borderRadius: '15px',
};



export default function PostDetail() {
    const [postDetails, setPostDetails] = useState([])
    const [authorInfo, setAuthorInfo] = useState([])
    const { post_id } = useParams()
    const { token } = useToken()
    const [authorName, setAuthorName] = useState("")
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    useEffect(() => {
        const fetchData = async () => {
        const url = `${REACT_APP_API_HOST}/api/posts/${post_id}`
        const response = await fetch(url, {
            "method": "GET",
            headers: { Authorization: `Bearer ${token}` }
        })

        if(response.ok) {
            const data = await response.json()
            setPostDetails(data)
        }
    }
    fetchData()
    }, [post_id, token]);

    const handleInterested = async () => {
        const url1 = `${REACT_APP_API_HOST}/api/posts/${post_id}/interested`
        await fetch(url1, {
            "method": "PUT",
            headers: { Authorization: `Bearer ${token}` }
        })
        const id = postDetails.author_id
        const url = `${REACT_APP_API_HOST}/api/accounts/${id}`
        const response = await fetch(url, {
            "method": "GET",
            headers: { Authorization: `Bearer ${token}` }
        })
        if(response.ok){
            const data = await response.json()
            setAuthorInfo(data)
            capitalizeFirstName(data)
            handleOpen()
        }
    }

    const capitalizeFirstName = async (data) => {
        const name = data.first_name
        const capitalized =
        name.charAt(0).toUpperCase()
        + name.slice(1)
        setAuthorName(capitalized)
    }

return (
    <>
      <h1 className="detailTagline">Meet Your New Best Friend Today</h1>
    <div className="topcontainer">
        <div className="detailCard">
          <div className="form">
            <div className="left-side">
              <h1>{postDetails.pet_name} </h1>
              <ul>
                <li>Species: {postDetails.species}</li>
                <li>Age: {postDetails.age}</li>
                <li>Size: {postDetails.pet_size}</li>
                <li>Location: {postDetails.location_city + ", " + postDetails.location_state + " " + postDetails.location_zipcode}</li>
                <li>{postDetails.interested} families are interested</li>
              </ul>
              <div>
                <div className="interestedButton">
                <Button onClick={handleInterested}>Interested</Button>
                <Modal
                  open={open}
                  onClose={handleClose}
                  aria-labelledby="modal-modal-title"
                  aria-describedby="modal-modal-description"
                >
                  <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                      Feel free to email {authorName} for more information on {postDetails.pet_name}!
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                      The best way to contact {authorName} would be to shoot them an email here: {authorInfo.email}.
                    </Typography>
                      <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                          We would like to thank you again for choosing to adopt!
                    </Typography>
                  </Box>
                </Modal>
                </div>
              </div>
            </div>
              <img className="right-side" src={postDetails.picture_url} alt="petimage"></img>

          </div>
        </div>
    </div>
    </>
  );
}
