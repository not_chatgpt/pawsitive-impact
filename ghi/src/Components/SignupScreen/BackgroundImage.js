import React from "react";

function BackgroundImage() {
  return (
    <div
      style={{
        backgroundImage:
          "url(https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fstatic.vecteezy.com%2Fsystem%2Fresources%2Fpreviews%2F001%2F864%2F666%2Flarge_2x%2Fsilhouette-of-man-and-dog-at-sunset-free-photo.JPG&f=1&nofb=1&ipt=b39a0daddf54bbbe78d6ff9302fef0487200503ac695fefab63f98ebfb887448&ipo=images)",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        backgroundPosition: "center",
        width: "58.5vw",
        height: "100%",
        position: "relative"
      }}
    />
  );
}

export default BackgroundImage;
