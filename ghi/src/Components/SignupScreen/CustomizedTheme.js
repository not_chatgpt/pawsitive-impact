import { createTheme } from "@mui/material/styles";

export default createTheme({
  palette: {
    mode: "dark",
    primary: {
      main: "#2CD7F8", // Sign up button
      contrastText: "#fff",
    },
    secondary: {
      main: "#fff",
    },
    background: {
      paper: "#000000",
      default: "#070510", // Form Background
    },
  },
  shape: {
    borderRadius: 4,
  },
});
