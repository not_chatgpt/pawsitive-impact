import React, { useState } from "react";
import logo1 from "./Logo/paws.png";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import { useNavigate } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";
const REACT_APP_API_HOST = process.env.REACT_APP_API_HOST;


function MainComponents() {
  const [userInputs, setInputs] = useState({
    first_name: "",
    last_name: "",
    email: "",
    username: "",
    password: "",
    checkpassword: "",
  });
  const navigate = useNavigate();
  const { register } = useToken();

  const [passwordsMatch, setPasswordsMatch] = useState(true);
  const [errorMessage, setErrorMessage] = useState("");


  function updateVar(event) {
    const name = event.target.name;
    const value = event.target.value;
    setInputs((prevVal) => ({ ...prevVal, [name]: value }));
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    // Add a check for password matching here
    if (userInputs.checkpassword === userInputs.password) {
      // Perform the registration or API call here
      // Assuming you have a function named register
      register(userInputs, `${REACT_APP_API_HOST}/api/accounts`);
      // Redirect to another page after successful registration
      navigate(`/`);
    } else {
      setPasswordsMatch(false);
      setErrorMessage("Passwords do not match!");
    }
  };

  // Render the component
  return (
    <div className="maincontainer">
      <img src={logo1} alt={"paws logo"} width={"100%"} />

      <div className="subdives">
        <h2>Create Your Profile</h2>
        <h6>Join the Pawsitive community!</h6>
      </div>

      <div className="textboxes">
        <TextField
          margin="normal"
          required
          fullWidth
          id="first_name"
          label="First Name"
          name="first_name"
          autoComplete="first_name"
          autoFocus
          size="small"
          onChange={updateVar}
        />
        <TextField
          margin="normal"
          required
          fullWidth
          id="last_name"
          label="Last Name"
          name="last_name"
          autoComplete="last_name"
          size="small"
          onChange={updateVar}
        />
        <TextField
          margin="normal"
          required
          fullWidth
          id="email"
          label="Email"
          name="email"
          type="email"
          autoComplete="email"
          size="small"
          onChange={updateVar}
        />
        <TextField
          margin="normal"
          required
          fullWidth
          id="username"
          label="Username"
          name="username"
          autoComplete="username"
          size="small"
          onChange={updateVar}
        />
        <TextField
          margin="normal"
          required
          fullWidth
          id="password"
          label="Password"
          name="password"
          type="password"
          autoComplete="password"
          size="small"
          onChange={updateVar}
        />
        <TextField
          margin="normal"
          required
          fullWidth
          id="checkpassword"
          label="Confirm Password"
          name="checkpassword"
          type="password"
          autoComplete="checkpassword"
          size="small"
          onChange={updateVar}
          />
          {!passwordsMatch && <p style={{ color: "red" }}>{errorMessage}</p>}
      </div>

      <div className="bottomsubdiv">
        <Button
          variant="contained"
          fullWidth
          my={2}
          onClick={handleSubmit}
          sx={{ textTransform: "none", height: "35px", borderRadius: "8px" }}
        >
          <h4 className="buttontexts">Sign up</h4>
        </Button>
        <div className="bottomcontainer">
          <h5>Already have an account?</h5>
          <a href="/login">
            <h5 id="logintext">
              <b>Login</b>
            </h5>
          </a>
        </div>
      </div>
    </div>
  );
}

export default MainComponents;
