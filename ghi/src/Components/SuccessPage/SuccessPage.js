import React, { useEffect, useState } from 'react';
import './SuccessPage.css'
const REACT_APP_API_HOST = process.env.REACT_APP_API_HOST;


function SuccessPage() {
    const [post, setPost] = useState([]);

    const fetchData = async () => {
        const response = await fetch(`${REACT_APP_API_HOST}/api/posts/adopted`);
        if (response.ok) {
            const data = await response.json();
            setPost(data);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
            <h1 className="successTagline"> Furry Friends, Forever Families, Celebrating Success</h1>
            <h1 className="successTagline2">Meet Our Adopted Stars</h1>
            <div className="container">
                {post.map((post) => {
                    return (
                        <div key={post.id} className="card">
                            <img src={post.picture_url} alt={post.pet_name} />
                            <div className="content">
                                <h2 className="title">{post.pet_name}</h2>
                            </div>
                        </div>
                    );
                })}
            </div>
        </>
    );
}

export default SuccessPage;
