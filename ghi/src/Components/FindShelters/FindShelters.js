import React, { useState, useEffect } from "react";

function FindShelters() {
    const [address, setAddress] = useState("");
    const [shelters, setShelters] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(null);
    const [animalFact, setAnimalFact] = useState("");
    const REACT_APP_API_HOST = process.env.REACT_APP_API_HOST;

    const fetchRandomAnimalFact = () => {
    fetch("https://dogapi.dog/api/v2/facts")
        .then((response) => response.json())
        .then((data) => {
        if (data && data.data && data.data.length > 0) {
            const randomFactData = data.data[0];
                setAnimalFact(randomFactData.attributes.body);
        }
        })
        .catch((error) => console.error("Error fetching animal fact", error));
    };

    useEffect(() => {
    fetchRandomAnimalFact();
    }, []);

    const handleSubmit = async (event) => {
    event.preventDefault();
    setIsLoading(true);
    setError(null);
    setShelters([]);

    try {
        const response = await fetch(
        `${REACT_APP_API_HOST}/list_shelters/?address=${address}&radius=16000`
        );

        if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
        }

        const data = await response.json();

        setShelters(data);
    } catch (e) {
        setError("Error fetching shelters. Please try again.");
    } finally {
        setIsLoading(false);
        fetchRandomAnimalFact();
    }
    };

    return (
    <div>
        <h1 style={{ fontFamily: "'Indie Flower', cursive" }}>Find Animal Shelters</h1>

        <div>
        <form onSubmit={handleSubmit}>
            <input
            value={address}
            onChange={(e) => setAddress(e.target.value)}
            placeholder="Enter address"
            />
            <button type="submit">Find Shelters</button>
        </form>
        </div>

        {isLoading ? (
        <div>
            <i className="fas fa-spinner fa-spin" style={{ fontSize: "24px" }}></i>
            <p>Fetching shelters, please wait...</p>
            <p>
            <b>Random Fact:</b> {animalFact}
            </p>
        </div>
        ) : null}

        {error && <p>{error}</p>}

        <ul>
            {shelters.map((shelter, index) => (
                <li key={index}>
                <h3>{shelter.Name}</h3>
                Address: {shelter.Address}
                <p>Phone: {shelter["Phone Number"]}</p>
                Website:{" "}
                <a href={shelter.Website} target="_blank" rel="noopener noreferrer">
                    {shelter.Website}
                </a>
                </li>
        ))}
        </ul>
    </div>
    );
}

export default FindShelters;
