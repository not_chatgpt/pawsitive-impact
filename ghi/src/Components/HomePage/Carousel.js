import React, { useState, useEffect } from "react";
import { Card, Stack } from "react-bootstrap";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import './Carousel.css'
import KeyboardDoubleArrowLeftIcon from '@mui/icons-material/KeyboardDoubleArrowLeft';
import KeyboardDoubleArrowRightIcon from '@mui/icons-material/KeyboardDoubleArrowRight';
import { useNavigate } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
const REACT_APP_API_HOST = process.env.REACT_APP_API_HOST;


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: 'none',
    boxShadow: 24,
    p: 4,
    borderRadius: '15px',
};



function PetCarousel() {
    const [posts, setPosts] = useState([]);
    const [currentSlide, setCurrentSlide] = useState(0);
    const navigate = useNavigate();
    const token = useToken()
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);



    const navigateToPostDetail = (postId) => {
        if (token.token) {
            navigate(`/main/adopt/${postId}/`);
        } else {
            handleOpen()
        }

    };



    useEffect(() => {
        const fetchData = async () => {
            const response = await fetch(`${REACT_APP_API_HOST}/api/posts/not-adopted/`);
            if (response.ok) {
                const data = await response.json();
                setPosts(data);
            }
        };

        fetchData();
    }, []);

    useEffect(() => {
        const interval = setInterval(() => {
            setCurrentSlide((prevSlide) =>
                prevSlide + 4 >= posts.length ? 0 : prevSlide + 4
            );
        }, 4000);

        return () => clearInterval(interval);
    }, [posts]);

    const responsive = {
        all: {
            breakpoint: { max: 3000, min: 0 },
            items: 4,
        },
    };

    const goToNextSlide = () => {
        setCurrentSlide((prevSlide) =>
            prevSlide + 4 >= posts.length ? 0 : prevSlide + 4
        );
    };

    const goToPreviousSlide = () => {
        setCurrentSlide((prevSlide) =>
            prevSlide - 4 < 0 ? posts.length - 4 : prevSlide - 4
        );
    };

    return (
        <>
            <div style={{ position: "relative", maxWidth: "100%" }}>
                <Carousel
                    responsive={responsive}
                    showDots={false}
                    arrows={false}
                    draggable={false}
                    swipeable={false}
                >
                    {posts.slice(currentSlide, currentSlide + 4).map((post, index) => (
                        <Stack key={index} direction="horizontal" gap={3}>
                            <div onClick={() => navigateToPostDetail(post.id)} variant="primary">
                                <Card style={{ width: "18rem", display: "flex", flexDirection: "column", alignItems: "center", justifyContent: "center" }}>
                                    <Card.Img
                                        variant="top"
                                        src={post.picture_url}
                                        alt="Slide"
                                        style={{
                                            width: "100%",
                                            height: "100%",
                                            objectFit: "cover",
                                        }} />
                                    <Card.Body>
                                        <Card.Title>{post.pet_name}</Card.Title>
                                    </Card.Body>
                                </Card>
                            </div>
                        </Stack>
                    ))}
                </Carousel>
                <KeyboardDoubleArrowLeftIcon
                    className="custom-carousel-button-prev"
                    style={{ fontSize: '2rem', cursor: 'pointer' }}
                    onClick={goToPreviousSlide}
                />
                <KeyboardDoubleArrowRightIcon
                    className="custom-carousel-button-next"
                    style={{ fontSize: '2rem', cursor: 'pointer' }}
                    onClick={goToNextSlide}
                />
            </div>
            <div>
                <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style}>
                        <Typography id="modal-modal-title" variant="h6" component="h2">
                            You must be signed in to see!
                        </Typography>
                    </Box>
                </Modal>
            </div>
        </>
    );
}

export default PetCarousel;
