const Footer = () => {
  const year = new Date().getFullYear();

  return <footer>{`Copyright © Pawsitive Impact ${year}`}</footer>;
};

export default Footer;