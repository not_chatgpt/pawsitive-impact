import { React, useEffect, useState } from 'react'
import AOS from 'aos';
import 'aos/dist/aos.css';
import PetsPhoto from "./petsPhoto1.png";
const REACT_APP_API_HOST = process.env.REACT_APP_API_HOST;

const SuccessStats = () => {
    const [posts, setPosts] = useState([]);

    const fetchData = async () => {
        const response = await fetch(`${REACT_APP_API_HOST}/api/posts/adopted/`);
        if (response.ok) {
            const data = await response.json();
            const numOfAdopted = data.length*4
            setPosts(numOfAdopted);
        }
    };

    useEffect(() => {

        AOS.init();
        AOS.refresh();
        fetchData();
    }, []);

    return (
        <>
            <div className="grid-container-successStats">
                <div className="grid-child purple">
                    <img
                        className="PetsPhoto1"
                        src={PetsPhoto}
                        alt="PetsPhoto"
                        data-aos="fade-up"
                    />
                </div>
                <div className="grid-child-green" data-aos="fade-down">
                    Celebrating the number of paws that have found their furever home: {"\n"}
                    {posts}
                </div>
            </div>
        </>
    );
};

export default SuccessStats;
