import React from "react";
import { NavLink } from 'react-router-dom';
import './Team.css'


function Team() {
  let message = `Where passion meets purpose. Meet our team and join us on our mission to make a pawsitive impact that's felt one paw at a time.`;
  return (
    <section className="section-white">
      <div className="container">
        <div className="row">
          <div className="col-md-12 text-center">
            <h2 className="section-title">Meet the Paws Behind the Code</h2>

            <p className="section-subtitle">{message}</p>
          </div>
          <section id="developers" className="developers section-bg">
            <div className="container" data-aos="fade-up">
              <div className="row gy-4 justify-content-center">
                <div className="col-lg-2 col-md-5 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                  <div className="developer-member">
                    <div className="member-img">
                      <img src="https://cdn.discordapp.com/attachments/1121845487872516181/1162461356365201518/image.png?ex=653c0580&is=65299080&hm=1b751c8d72bc2a95d758da8d84d70ec35394d15cb844067e9cb691613c836205&" className="img-fluid" alt="" />
                      <div className="social">
                        <NavLink to="https://gitlab.com/minastowe"><i className="bi bi-github"></i></NavLink>
                        <NavLink to="https://www.linkedin.com/in/minastowe/"><i className="bi bi-linkedin"></i></NavLink>
                      </div>
                    </div>
                    <div className="member-info">
                      <h4>Mina Nguyen</h4>
                    </div>
                  </div>
                </div>
                <div className="col-lg-2 col-md-5 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
                  <div className="developer-member">
                    <div className="member-img">
                      <img src="https://cdn.discordapp.com/attachments/1121845487872516181/1162461356365201518/image.png?ex=653c0580&is=65299080&hm=1b751c8d72bc2a95d758da8d84d70ec35394d15cb844067e9cb691613c836205&" className="img-fluid" alt="" />
                      <div className="social">
                        <NavLink to="https://gitlab.com/LinhNgo0023"><i className="bi bi-github"></i></NavLink>
                        <NavLink to="https://www.linkedin.com/in/linh-ngo0023/"><i className="bi bi-linkedin"></i></NavLink>
                      </div>
                    </div>
                    <div className="member-info">
                      <h4>Linh Ngo</h4>
                    </div>
                  </div>
                </div>
                <div className="col-lg-2 col-md-5 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="300">
                  <div className="developer-member">
                    <div className="member-img">
                      <img src="https://cdn.discordapp.com/attachments/1121845487872516181/1162461356365201518/image.png?ex=653c0580&is=65299080&hm=1b751c8d72bc2a95d758da8d84d70ec35394d15cb844067e9cb691613c836205&" className="img-fluid" alt="" />
                      <div className="social">
                        <NavLink to="https://gitlab.com/birasluiz"><i className="bi bi-github"></i></NavLink>
                        <NavLink to="https://www.linkedin.com/in/biraluiz/"><i className="bi bi-linkedin"></i></NavLink>
                      </div>
                    </div>
                    <div className="member-info">
                      <h4>Bira Luiz</h4>
                    </div>
                  </div>
                </div>
                <div className="col-lg-2 col-md-5 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="300">
                  <div className="developer-member">
                    <div className="member-img">
                      <img src="https://cdn.discordapp.com/attachments/1121845487872516181/1162461356365201518/image.png?ex=653c0580&is=65299080&hm=1b751c8d72bc2a95d758da8d84d70ec35394d15cb844067e9cb691613c836205&" className="img-fluid" alt="" />
                      <div className="social">
                        <NavLink to="https://gitlab.com/WestonGambardella"><i className="bi bi-github"></i></NavLink>
                        <NavLink to="https://www.linkedin.com/in/weston-gambardella/"><i className="bi bi-linkedin"></i></NavLink>
                      </div>
                    </div>
                    <div className="member-info">
                      <h4>Weston Gambardella</h4>
                    </div>
                  </div>
                </div>
                <div className="col-lg-2 col-md-5 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="300">
                  <div className="developer-member">
                    <div className="member-img">
                      <img src="https://cdn.discordapp.com/attachments/1121845487872516181/1162461356365201518/image.png?ex=653c0580&is=65299080&hm=1b751c8d72bc2a95d758da8d84d70ec35394d15cb844067e9cb691613c836205&" className="img-fluid" alt="" />
                      <div className="social">
                        <NavLink to="https://gitlab.com/Efren.Acosta"><i className="bi bi-github"></i></NavLink>
                        <NavLink to="https://www.linkedin.com/in/efren-acosta/"><i className="bi bi-linkedin"></i></NavLink>
                      </div>
                    </div>
                    <div className="member-info">
                      <h4>Efren Acosta</h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

        </div>
      </div>
    </section>
   );
 }

 export default Team;
