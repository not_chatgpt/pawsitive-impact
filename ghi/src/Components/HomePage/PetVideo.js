import React from "react";
import videoBG from "./pet-videos.mp4";
import { Link } from 'react-router-dom';


const PetVideo = () => {

  return (
    <section id="mainBG" className="mainBG d-flex align-items-center section-bg">
    <div className="container video" >
    <video src={ videoBG } autoPlay loop muted />
        <div className="text-container px-4 py-5 my-5 text-center">
        <div className="col-lg-6 mx-auto">
            <h2 className="lead mb-4">
            Bring your best friend home today!
            </h2>
            <Link to="/main/adopt">
              <button type="submit" className="video-adopt-btn  type--A">
                <div className="video-adopt-btn__line"></div>
                <div className="video-adopt-btn__line"></div>
                <span className="video-adopt-btn__text">Adopt</span>
                <div className="video-adopt-btn__drow1"></div>
                <div className="video-adopt-btn__drow2"></div>
              </button>
            </Link>
        </div>
        </div>
    </div>
  </section>
  );
};

export default PetVideo;
