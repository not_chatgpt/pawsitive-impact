import React, { useState, useEffect, useCallback } from "react";
import PetCarousel from "./Carousel";
import SuccessStats from "./SuccessStats";
import Team from "./Team";
import './HomePage.css'
import PetVideo from "./PetVideo";
import videoBG from "./pet-videos.mp4";
import Footer from "./Footer";
import AOS from 'aos';
import 'aos/dist/aos.css';
const REACT_APP_API_HOST = process.env.REACT_APP_API_HOST;


function HomePage() {
    const [spotlightPost, setSpotlightPost] = useState(null);


    const fetchData = useCallback(async () => {
        const response = await fetch(`${REACT_APP_API_HOST}/api/posts/not-adopted`);
        if (response.ok) {
        const data = await response.json();
        const randomIndex = Math.floor(Math.random() * data.length);
        setSpotlightPost(data[randomIndex]);
        }
    }, [setSpotlightPost]);

    useEffect(() => {
        fetchData()
        AOS.refresh();
    }, [fetchData]);

    return (
        <>
            <PetVideo videoSource={videoBG} />
        <div className="homepage-container">
        <div className="spotlight-section">
            {spotlightPost && (
            <div className="spotlight-card">
                <div className="spotlight-card-body">
                <h2>All paws on deck – check me out!</h2>
                <img
                    className= "spotlight-img"
                    src={spotlightPost.picture_url}
                    alt={spotlightPost.pet_name}
                />
                <h3>{spotlightPost.pet_name}</h3>
                <p>{spotlightPost.pet_description}</p>
                </div>
            </div>
            )}
        </div>
        <div className="carousel-section">
            <h2>Our friends</h2>
            <h3>who are looking for their fur-ever home</h3>
            <PetCarousel></PetCarousel>
        </div>
        <div
            className="text-section"
            data-aos="zoom-in"
            data-aos-delay="1000"
        >
            <h2>Our Mission</h2>
            <p>
            At Pawsitive Impact, our mission is to connect loving homes with
            deserving pets, fostering a brighter future for both. We are dedicated
            to promoting responsible pet adoption, advocating for animal welfare,
            and cultivating a world where every paw finds its place. Our
            commitment to compassion, education, and community engagement empowers
            individuals to make a lasting, positive impact on the lives of
            animals. Together, we envision a world where every wag of a tail and
            purr of contentment reflects the Pawsitive Impact we strive to create.
            </p>
        </div>
        </div>
        <div>
            <SuccessStats />
            <Team />
        </div>
        <div className="footer">
            <Footer />
        </div>
        </>
    );
}

export default HomePage;
