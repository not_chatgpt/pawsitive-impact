import React, { useState } from "react";
import logo1 from "./Logo/paws.png";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import { useNavigate } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";

function MainComponents1() {
  const [userInputs, setInputs] = useState({
    username: "",
    password: "",
  });
  const navigate = useNavigate();
  const { login } = useToken();




  function updateVar(event) {
    const name = event.target.name;
    const value = event.target.value;
    setInputs((prevVal) => ({ ...prevVal, [name]: value }));
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
      login(userInputs.username, userInputs.password);
      navigate(`/`);
  };

  return (
    <div className="maincontainer">
      <img src={logo1} alt={"paws logo"} width={"100%"} />

      <div className="subdives">
        <h2>Log In To Your Profile!</h2>
      </div>
      <div className="textboxes">
        <TextField
          margin="normal"
          required
          fullWidth
          id="username"
          label="Username"
          name="username"
          autoComplete="username"
          size="small"
          onChange={updateVar}
        />
        <TextField
          margin="normal"
          required
          fullWidth
          id="password"
          label="Password"
          name="password"
          type="password"
          autoComplete="password"
          size="small"
          onChange={updateVar}
        />
      </div>

      <div className="bottomsubdiv">
        <Button
          variant="contained"
          fullWidth
          my={2}
          onClick={handleSubmit}
          sx={{ textTransform: "none", height: "35px", borderRadius: "8px" }}
        >
          <h4 className="buttontexts">Log in</h4>
        </Button>
      </div>
    </div>
  );
}

export default MainComponents1;
