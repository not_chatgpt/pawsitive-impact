import React from "react";

function BackgroundImage1() {
  return (
    <div
      style={{
        backgroundImage:
          "url(https://www.paradisecoast.com/sites/default/files/styles/hero/public/header-stephanie-buffington-rii48dz7cti-unsplash.jpg?itok=j-4iEseH)",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        backgroundPosition: "center",
        width: "58.5vw",
        height: "100%",
        position: "relative",
      }}
    />
  );
}

export default BackgroundImage1;
