import React from "react";
import MainComponents1 from "./TextButtons1";
import Grid from "@mui/material/Grid";
import CssBaseline from "@mui/material/CssBaseline";
import { ThemeProvider } from "@mui/material/styles";
import "./login_screen.css";
import theme2 from "./CustomizedTheme"
import BackgroundImage1 from "./BackgroundImage1";




function Login() {
  return (
    <>
      <ThemeProvider theme={theme2}>
        <Grid container component="main" sx={{ height: "100vh" }}>
          <CssBaseline />

          <Grid item xs={12} sm={7} md={4}>
            <MainComponents1 />
          </Grid>

          <Grid item xs={false} sm={5} md={8}>
            <BackgroundImage1 />
          </Grid>
        </Grid>
      </ThemeProvider>
    </>
  );
}

export default Login;
