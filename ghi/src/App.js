import React from "react";
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import HomePage from "./Components/HomePage/HomePage";
import Nav from "./Components/Nav/Nav";
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import PostDetail from "./Components/PostDetails/PostDetail";
import PostForm from "./Components/Account/PostForm";
import Adopt from "./Components/Adopt/Adopt";
import SuccessPage from "./Components/SuccessPage/SuccessPage";
import SignUp from "./Components/SignupScreen/Signup";
import UpdateAccount from "./Components/Account/UpdateAccount";
import AccountPage from "./Components/Account/AccountPage";
import UpdatePost from "./Components/Account/UpdatePost";
import FindShelters from "./Components/FindShelters/FindShelters";
import Login from "./Components/Login/Login";
import Logout from "./Components/Login/Logout";

const REACT_APP_API_HOST = process.env.REACT_APP_API_HOST
const domain = /https:\/\/[^/]+/;
const basename = process.env.PUBLIC_URL.replace(domain, '');

export default function App() {
  return (
    <AuthProvider baseUrl={REACT_APP_API_HOST}>
      <Router basename={basename}>
        <Nav />
        <div className="container">
          <Routes>
            <Route path="/" element={<HomePage />} />
            <Route path="find-shelters/" element={<FindShelters />} />
            <Route path="success-stories/" element={<SuccessPage />} />
            <Route path="sign-up/" element={<SignUp />} />
            <Route path="login/" element={<Login />} />
            <Route path="logout/" element={<Logout />} />
            <Route path="/main/adopt">
              <Route path="" element={<Adopt />} />
              <Route path="success-stories/" element={<SuccessPage />} />
              <Route path=":post_id/" element={<PostDetail />} />
            </Route>
            <Route path="/main/account-page/">
              <Route path="" element={<AccountPage />} />
              <Route path="update-account/" element={<UpdateAccount />} />
              <Route path="update-post/:post_id" element={<UpdatePost />} />
              <Route path="post-form/" element={<PostForm />} />
            </Route>
          </Routes>
        </div>
      </Router>
    </AuthProvider>
  );
}
