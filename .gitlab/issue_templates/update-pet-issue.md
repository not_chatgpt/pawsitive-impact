#### Title:

##### Story

```
As a user that is updating information on the update/delete form of a pet
When I enter information in the correct fields and hit update
I want it to save the changed information and display it on the details page.
```

##### Feature

```
Given that I am a logged in user on the update/delete a pet form page
I want to be able to edit the form and click update or delete
Then the updated information is changed in the database
And changes can be saved and displayed on the front end of details pages
```

##### Description

* While the user is logged in, the form should be filled and the "update" and "delete" button is visible
* When the user enters in information into the fields and clicks the "update" button the page should refresh
* When the user has updated the information of the form it is also updated on the database
    * animal_name should be the value in the returned data
    * animal_age should be the value in the returned data
    * animal_color should be the value in the returned data
    * animal_species should be the value in the returned data
    * animal_image should be the value in the returned data
    * created_on should be the value in the returned data
    * description should be the value in the returned data
    * location:
        * city should be the value in the returned data
        * state should be the value in the returned data
        * zip_code should be the value in the returned data
    * author should be the value in the returned data
    * adopted should be the value in the returned data
* After all the fields have been updated there should be a 200 response is sent to the frontend and the user is redirected to the details page.
* If the page is not created successfully there should be a 500 response sent to the frontend


### Description:

- While the user is logged in, the form should be filled and the "update" button is visible
- When the user enters in information into the fields and clicks the "update" button the page should refresh
- When the user has updated the information of the form they should receive a 200 response, and the information is updated on the database


##### Technical description

- This should be handled in a update_post method attached the Post Route using the endpoint of 'api/list-posts/{post_id}'
    - This update_post method will update a specific instance in our post database

### Acceptance Criteria:

- As a logged in user,I can change the information in the displayed field and be able to click on the "update" button and be redirected to the details page and the information will be updated in the database,
- This sends a request to the backend which creates a row in the DB
- A 200 response is sent to the frontend and the user is redirected to that specific details page and should be able to see the changes

### Definition of Done:

- [ ] Source code
- [ ] Unit test(s) (if needed)
- [ ] Passing CI/CD pipeline(s)
- [ ] Approved and completed merge request(s)
