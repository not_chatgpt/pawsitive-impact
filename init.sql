CREATE TABLE IF NOT EXISTS accounts (
    id SERIAL PRIMARY KEY,
    first_name VARCHAR(50),
    last_name VARCHAR(50),
    email VARCHAR(100) NOT NULL,
    username VARCHAR(15) UNIQUE NOT NULL,
    hashed_password VARCHAR(200) NOT NULL
);

CREATE TABLE IF NOT EXISTS posts (
    id SERIAL PRIMARY KEY,
    pet_name VARCHAR(100) NOT NULL,
    species VARCHAR(100) NOT NULL,
    color VARCHAR(50),
    age VARCHAR(100),
    picture_url VARCHAR(500) NOT NULL,
    pet_size VARCHAR(20) NOT NULL,
    location_city VARCHAR(100) NOT NULL,
    location_state VARCHAR(100) NOT NULL,
    location_zipcode INT NOT NULL,
    pet_description TEXT NOT NULL,
    is_adopted BOOLEAN NOT NULL,
    author_id INT NOT NULL REFERENCES accounts(id),
    interested INT
);
